# Langages : exercice 4
 
[Exercice qui associe à chaque code, une réponse parmi la liste donnée
Attention : certains code ont plusieurs réponses consécutives à fournir (une par print)]

1) Qu'imprime les code suivant ?


```python
  x = 7
  z = -1
  def f():
      x = 42        
      def g():
          nonlocal x
          x+=1      
      g()           
      global y      
      y = x * z     
  f()
  print(x, y, z)    
```

- Réponse correcte :  # 7 -43 -1

Possibilités: 

- 7 -43 -1
- 7 -8 -1
- 43 -43 -1
- 8 -8 -1  

2)  Qu'imprime les code suivant ?

```Python
x = 7
def f():
    global y 
    y = 2 * x
f()
x = "abc"    
f()
print(x, y)  
```

- Réponse correcte :    abc abcabc


Possibilités

- abc 14
- abc abcabc


3) Soit le code suivant :

```python
def push(a, L=[]):
    L.append(a)
    return L

a = 3
S = [2, 1, 7]
print(push(a, S)) # 1er print 
print(S)          # 2ème print
a = 8
print(push(a))    # 3ème print
print(S)          # 4ème print 
a = -1
print(push(a))    # 5ème print
```

Qu'affiche chaque print ?


- 1er print : [2, 1, 7, 3]
- 2ème print : [2, 1, 7, 3] 
- 3ème print : [8]
- 4ème print : [2, 1, 7, 3]
- 5ème print : [8, -1]

Possibilités :

- []
- [8]
- [-1]
- [8, -1]
- [2, 1, 7]
- [2, 1, 7, 3]

4) Soit le code suivant :

```python
def push(a, L=None):
    L = [] if L is None else L
    L.append(a)
    return L

a = 3
S = [2, 1, 7]
print(push(a, S)) # 1er print
print(S)          # 2ème print 
a = 8
print(push(a))    # 3ème print
a = -1
print(push(a))    # 4ème print 
```

Qu'affiche chaque print ?

- 1er print : [2, 1, 7, 3]
- 2ème print : [2, 1, 7, 3]
- 3ème print : [8]
- 4ème print : [-1]

Possibilités :

- []
- [8]
- [-1]
- [8, -1]
- [2, 1, 7]
- [2, 1, 7, 3]

