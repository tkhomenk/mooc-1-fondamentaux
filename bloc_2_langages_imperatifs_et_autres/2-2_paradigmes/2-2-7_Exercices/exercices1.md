# Langages : exercice 1

[Exercice qui associe à chaque langage, un ensemble de caractéristiques parmi la liste donnée]

Classez les langages suivants en fonction :

- de la  décénie pendant laquelle ils ont été définis 
- des paradigmes qu'ils contiennent
- le premier usage  auquel ils étaient destinés
- du fait qu'ils soient compilés ou interprétés
- du fait qu'ils sont (plutôt) faiblement ou fortement typés
- du fait que leur contrôle de type est (plutôt) statique ou dynamique

Possibilités :

- 1950
- 1960
- 1970
- 1980
- 1990
- 2000
- 2010
- 2020
- impératif
- programmation scientifiques
- compilé
- faiblement typé
- contrôle statique
- programmation en gestion
- fortement typé
- Orienbté-Objet
- Fonctionnel
- programmation de scripts
- interprété
- contrôle dynamique 
- programmation système
- programmation généraliste
- semi-compilé
- enseignement et recherche
- développement d'applications


- FORTRAN, 1957, impératif, programmation scientifiques, compilé,  faiblement typé, contrôle statique
- COBOL, 1959, impératif, programmation en gestion, compilé,  fortement typé, contrôle statique
- Python, 1990, impératif, Orienbté-Objet, Fonctionnel,programmation de scripts, interprété, fortement typé, contrôle dynamique 
- C, 1970, impératif, programmation système, compilé, faiblement typé, contrôle statique
- C++,  1983, impératif, orienté-objet, fonctionnel, programmation généraliste, compilé, faiblement typé, contrôle statique
- Java, 1995, impératif, orienté-objet, fonctionnel, développement d'applications, semi-compilé, fortement typé, contrôle statique  
- OCaml, 1996, impératif, orienté-objet, fonctionnel, enseignement et recherche, compilé, fortement typé, contrôle statique  
- Basic, 1964, impératif, programmation généraliste, interprété, fortement typé, contrôle dynamique
- Ruby, 1995, orienté-objet, impératif, fonctionnel, programmation généraliste, interprété, fortement typé, contrôle dynamique  
- PHP, 1994, impératif, orienté-objet, fonctionnel, programmation de scripts,interprété, faiblement typé, contrôle dynamique  
- Javascript  1995, orienté-objet, impératif, fonctionnel, programmation de scripts ,interprété, faiblement typé, contrôle dynamique  


