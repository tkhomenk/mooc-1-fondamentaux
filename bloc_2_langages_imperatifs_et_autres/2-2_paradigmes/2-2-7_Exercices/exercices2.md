# Langages : exercice 2

## Proposition de questionnalisation par Thierry 

Q1: Mettre dans le bon ordre ces étapes pour passer du code source à un code exécutable:

- Analyse lexicale
- Analyse syntaxique
- Analyse sémantique
- Génération d'un code intermédiaire
- Optimisation du code
- Génération de code final
- Édition de lien
  - ... qui sont à presenter en desordre]

Explication:  

- Analyse lexicale (également appelé **scanning** en anglais), phase de la compilation qui découpe la séquence de caractères du code source en blocs atomiques appelés jetons (tokens en anglais). Chaque jeton appartient à une unité atomique unique du langage appelée unité lexicale ou lexème (par exemple un mot-clé, un identifiant ou un symbole).
- Analyse syntaxique (également appelé **parsing** en anglais) , phase de la compilation qui implique l’analyse de la séquence de jetons pour identifier la structure syntaxique du programme. Cette phase s’appuie généralement sur la construction d’un arbre d’analyse (appelé communément arbre syntaxique) ; on remplace la séquence linéaire des jetons par une structure en arbre construite selon la grammaire formelle qui définit la syntaxe du langage. Par exemple, une condition est toujours suivie d’un test logique (égalité, comparaison, ...). L’arbre d’analyse est souvent modifié et amélioré au fur et à mesure de la compilation.
- Analyse sémantique, phase durant laquelle le compilateur ajoute des informations sémantiques (donner un « sens ») à l’arbre syntaxique. Cette phase fait un certain nombre de contrôles: vérifie le type des éléments (variables, fonctions, ...) utilisés dans le programme, leur visibilité, vérifie que toutes les variables locales utilisées sont initialisées, .... 
- Génération du code intermédiaire, phase durant laquelle est produit un code du programme dans un langage de bas niveau "standard" (a priori pas le langage cible final).
- Optimisation, phase durant laquelle on profite de toutes les informations déduites précédemment pour éliminer les parties inutiles du code, essayer de réarranger certaines parties, voir à profiter des accélérations matérielles possibles, etc.
- Partie finale du compilateur, phase qui comprend la génération de code final.
- Edition des liens, phase après la compilation qui réalise  l’intégration en un code complet de tous ces éléments. Elle permet non seulement de relier un programme avec certains modules de la bibliothèque, mais également de combiner plusieurs codes objet correspondant à des morceaux de programme, éventuellement écrits dans des langages différents, qui ont fait l’objet d’une compilation séparée.

Q2: Associer au chaque typage à sa definition
- Typage statique
Caractéristique d'un langage de programmation qui exprime que la détermination du type et la vérification de la faisabilité de l’opération, moyennant d’éventuelles conversions supplémentaires, sont effectuées dès la compilation.
- Typage dynamique
Caractéristique d'un langage de programmation  qui exprime que la détermination du type et la vérification de la faisabilité de l’opération, moyennant d’éventuelles conversions supplémentaires, sont effectuées lors de l’exécution.
- Typage fort
Caractéristique d'un langage de programmation qui exprime que 
 son contrôle de type est suffisamment strict et riche pour procurer une vérification sûre de la cohérence du programme écrit et l’insertion non ambiguë de conversions efficaces.
- Typage faible
Caractéristique d'un langage de programmation qui exprime que 
 son contrôle de type n'est pas suffisamment strict et riche pour procurer une vérification sûre de la cohérence du programme écrit et l’insertion non ambiguë de conversions efficaces.

Q3 : À quoi ne sert pas un ramasse-miette ? (deux réponses exactes)
+ À détecter les zones mémoire devenues inaccessibles 
+ À réutiliser une zone mémoire plusieurs fois au fil de l'exécution du programme
- À réduire l'utilisation mémoire du programme.
+ À automatiser la gestion de la mémoire pour le programmeur
+ À accélérer la gestion de la mémoire pour le programmeur

Explication: Ramasse-miettes (garbage collector) :  algorithme  de détection des zones mémoire devenues inaccessibles qui peuvent a priori être réutilisées à d’autres fins. Ce processus est appelé automatiquement, à intervalles réguliers ou en cas de manque de place apparent, et de façon transparente pour le programmeur. Toutefois, il est souvent possible de le lancer  explicitement : c’est une des fonctions de la bibliothèque.

Q4 : Ramasse miette et typage fort ou faible, quelle est la réponse exacte ?
- Quelque soit le typage des objets on peut correctement identifier toutes les références à un objet inutilisé.
- Avec le typage fort les récupérateurs peuvent correctement identifier toutes les références à un objet inutilisé.
- Même avec un typage fort les récupérateurs il y a toujours des références à des objets (par exemple cycliques) qui ne peuvent être indentifiés.

Explication : [Comme expliqué ici](https://fr.wikipedia.org/wiki/Ramasse-miettes_(informatique)#D%C3%A9finition_et_fonctionnement) oui il faut un typage fort et dans ce cas on peut identifier les références aux ojets mêmes cycliques on parle de récupérateur exact.
- 



## Matériel fourni par Thierry

[Exercice qui associe à chaque terme, une définition ou explication parmi la liste donnée]

Retrouver à quoi correspond chaque terme donné ici:

- Analyse lexicale
- Analyse syntaxique
- Analyse sémantique
- Table des symboles
- Optimisation**
- Génération de code final
- Partie frontale du compilateur
- Partie finale du compilateur
- Edition des liens
- Typage statique
- Typage dynamique
- Typage fort
- Typage faible
- Ramasse-miettes



- Analyse lexicale
(également appelé **scanning** en anglais), phase de la compilation qui découpe la séquence de caractères du code source en blocs atomiques appelés jetons (tokens en anglais). Chaque jeton appartient à une unité atomique unique du langage appelée unité lexicale ou lexème (par exemple un mot-clé, un identifiant ou un symbole).


- Analyse syntaxique
(également appelé **parsing** en anglais) , phase de la compilation qui implique l’analyse de la séquence de jetons pour identifier la structure syntaxique du programme. Cette phase s’appuie généralement sur la construction d’un arbre d’analyse (appelé communément arbre syntaxique) ; on remplace la séquence linéaire des jetons par une structure en arbre construite selon la grammaire formelle qui définit la syntaxe du langage. Par exemple, une condition est toujours suivie d’un test logique (égalité, comparaison, ...). L’arbre d’analyse est souvent modifié et amélioré au fur et à mesure de la compilation.


- Analyse sémantique
- phase durant laquelle le compilateur ajoute des informations sémantiques (donner un « sens ») à l’arbre syntaxique. Cette phase fait un certain nombre de contrôles: vérifie le type des éléments (variables, fonctions, ...) utilisés dans le programme, leur visibilité, vérifie que toutes les variables locales utilisées sont initialisées, .... 

- Table des symboles
 centralise les informations attachées aux identificateurs du programme. On y retrouve des informations comme le type, l'emplacement mémoire, la portée, la visibilité, etc.


- Partie frontale du compilateur
 Partie du compilateur qui comprend l’analyse lexicale, syntaxique et sémantique, le passage par un langage intermédiaire et l’optimisation.

- Partie finale du compilateur
Partie du compilateur qui comprend la génération de code final

- Edition des liens
Phase après la compilation qui réalise  l’intégration en un code complet de tous ces éléments. Elle permet non seulement de relier un programme avec certains modules de la bibliothèque, mais également de combiner plusieurs codes objet correspondant à des morceaux de programme, éventuellement écrits dans des langages différents, qui ont fait l’objet d’une compilation séparée.

- Typage statique
Caractéristique d'un langage de programmation qui exprime que la détermination du type et la vérification de la faisabilité de l’opération, moyennant d’éventuelles conversions supplémentaires, sont effectuées dès la compilation.
- Typage dynamique
Caractéristique d'un langage de programmation  qui exprime que la détermination du type et la vérification de la faisabilité de l’opération, moyennant d’éventuelles conversions supplémentaires, sont effectuées lors de l’exécution.

- Typage fort
Caractéristique d'un langage de programmation qui exprime que 
 son contrôle de type est suffisamment strict et riche pour procurer une vérification sûre de la cohérence du programme écrit et l’insertion non ambiguë de conversions efficaces.

- Typage faible
Caractéristique d'un langage de programmation qui exprime que 
 son contrôle de type n'est pas suffisamment strict et riche pour procurer une vérification sûre de la cohérence du programme écrit et l’insertion non ambiguë de conversions efficaces.

- Ramasse-miettes
(garbage collector) :  algorithme  de détection des zones mémoire devenues inaccessibles qui peuvent a priori être réutilisées à d’autres fins. Ce processus est appelé automatiquement, à intervalles réguliers ou en cas de manque de place apparent, et de façon transparente pour le programmeur. Toutefois, il est souvent possible de le lancer  explicitement : c’est une des fonctions de la bibliothèque.

