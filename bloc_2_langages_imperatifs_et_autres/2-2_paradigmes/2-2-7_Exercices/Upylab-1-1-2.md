# Exercice UpyLaB de codage en Python

## Enoncé

Ecrivez en Python, une fonction `representation_fractionnaire(val, base=2, longueur=128)`

qui renvoit la représentation en base base de la valeur fractionnaire 'val'

### Hypothèses 

- `0.0 <= val < 1.0`
- `1 < base < 37`

### Résultat

Renvoit une chaîne de caractères `"0. ..."` qui représente:

- soit '0.0' si val vaut 0.0
- soit la valeur exacte dans la base `base`
- soit la représentation de valeur approché avec une partie décimale de `longueur` chiffres et dans ce cas, la représentation est terminée par un caractère '+'
- soit la valeur avec la représentation notée '0.prefixe (periodique)$\; \omega$' et représentant la représentation

'0. prefixe periodique periodique ...'

(la partie periodique étant répétée une infinité de fois.

Les chiffres seront notés (dans l'ordre des valeurs) : `'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'`

### Paramètres
- `val` : `float` (`0.0 <= val < 1.0`) valeur fractionnaire à représenter
- `base`: `int` (`1 < base < 37`) base

### Conseil

$\omega$ est donné en Python par `chr(969)` 

### Résultat

Renvoie, suivant les paramètres une chaîne de caractères parmi les formats :

- '0.0' 
- '0. prefixe' 
- '0. prefixe +'
- '0. prefixe (periodique) $\; \omega$'

(voir plus haut).


### Exemples :

- representation_fractionnaire(0.0, 20) : '0.0'
- representation_fractionnaire(0.1, 5) : '0.0(2)$\omega$' # représente 0.02222222...
- representation_fractionnaire(0.1, 20) : '0.2' # en base 20
- representation_fractionnaire(1/3, 10, 20) : '0.33333333333333330372+' # non terminé
(dû aux erreurs dans la représentation de 1/3)