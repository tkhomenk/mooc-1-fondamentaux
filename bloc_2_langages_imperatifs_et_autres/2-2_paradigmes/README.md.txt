# Sommaire B2-M2

## 2-2 Présentation du module

## 2-2-1 Paradigmes des langages de programmation

## 2-2-2 Compilateur et interpréteur

## 2-2-3 Les langages impératifs

## 2-2-4 Typage des langages de programmation

## 2-2-5 Fonctions et modules

## 2-2-6 Histoire et taxonomie des Langages de programmation
