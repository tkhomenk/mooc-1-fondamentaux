# Manipulations d'objets

Le but de cet exercice en plusieurs questions est de manipuler quelques objets simples. Ce seront des _playlist_ de titres de musique et des durées. Après avoir réalisé l'exercice, vous répondrez aux questions du quiz qui suit.

## Objet `Duree`

On souhaite manipuler des durées sous la forme _heure_, _minutes_ et _secondes_. 

### Question 1

Créer un objet `Duree` comportant les attributs `heure`, `min` et `sec`. Voici quelques exemples de création d'instances de cet objet :

```python
>>> d0 = Duree(5, 34, 12)
>>> d1 = Duree(0, 70, 60)
>>> d1
Duree(1, 11, 0)
```

On vous donne la méthode `__repr__` (celle qui est invoquée lorsqu'on demande à l'interprète de nous évaluer l'instance `d1` ci-dessus par exemple) :

```python
def __repr__(self):
    return f'Duree({self.heure}, {self.min:02}, {self.sec:02})'
```

### Question 2

Il nous faudra pouvoir comparer des durées. Écrire les trois méthodes pour les opérateurs supérieur, inférieur et égal :

```python
def __gt__(self, autre_duree):
    # à compléter

def __lt__(self, autre_duree):
    # à compléter

def __eq__(self, autre_duree):
    # à compléter
```

### Question 3

Écrire la méthode `__add__` permettant d'additionner des durées : `d0 + d1` doit créer une 3e durée qui est la somme des durées `d0` et `d1` :

```python
>>> Duree(0, 0, 34) + Duree(0, 0, 52)
Duree(0, 1, 26)
```

### Question subsidiaire

Dans la suite, on aura besoin de créer une durée via une chaîne de caractères de la forme `heures:minutes:secondes`. On pourra définir une méthode de classe `duree`  qui accepte une chaîne de caractères représentant des heures, des minutes et des secondes séprarées par un caractère. Par exemple :

```python
>>> Duree.duree('6:19:20')
Duree(6, 19, 20)
>>> Duree.duree('0:234:7665')
Duree(6, 1, 45)
```

_Note_ : le début de la définition de cette méthode est donnée dans les indications disponibles en bas de la page. 


## Objet `Titre` 

Un titre musical est un objet qui comporte les attributs suivants :

- `titre` : le titre du morceau, il s'agit d'une chaîne de caractères
- `album` : le nom de l'album dont est extrait le morceau (également une chaîne de caractères)
- `annee` : l'année de sortie de l'album (toujours une chaînes de caractères)
- `duree` : la durée du morceau (un objet `Duree`)

Xuan, Bob et Inaya sont trois ami-es qui ont en commun de n'écouter que des titres du groupe [Pink Floyd](https://fr.wikipedia.org/wiki/Pink_Floyd).

Le fichier csv [`pink_floyd_durees.csv`](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/__supports-de-cours/B2-M1/pink_floyd_durees.csv) contient quelques titres du célèbre groupe de rock. Téléchargez ce fichier.

### Question 4 

À partir de ce fichier, créez la constante `PINK_FLOYD` qui contient la liste des objets `Titre` correspondant aux lignes du fichier, dans le même ordre.

### Question 5

Une `PlayList` est constituée d'une `base` (une liste de `Titre`) et d'une `ids`, ensemble (au sens `set` de Python) d'indices, correspondant aux titres de la `base` présents dans la _playlist_

Définir la classe `PlayList`. Y ajouter les méthodes suivantes :

- `titre` : qui prend un numéro de titre (un _id_) et qui renvoie le `Titre` (au sens objet) correspondant de la _playlist_
- `duree` : qui envoie la durée total (sous la forme d'un objet `Duree`) de la _playlist_
- `commun` : qui prend une autre `PlayList` en paramètre et crée une troisième `PlayList` constituée des `Titres` que les deux _playlists_ ont en commun.
- `__str__` qui permet d'afficher une `PlayList` ; comme ceci (entre parenthèses on trouve le nom de l'album et l'année) :

    ```python
    >>> small = PlayList(PINK_FLOYD, {45, 32, 120})
    >>> print(small)
    ```

    ```
     32 [01:06] Unsung (The Endless River, 2014)
    120 [12:48] A Saucerful of Secrets (Ummagumma, 1969)
     45 [03:19] In the Flesh ? (The Wall, 1979)
    ```

### Question 6

Ci-dessous les _ids_ de Xuan, Bob et Inaya :

```python
XUAN = {139, 88, 107, 57, 14, 100, 44, 129, 45, 8, 53, 138, 125, 105, 151, 98, 80, 40, 131, 128, 29, 122, 134, 150, 17, 19, 90, 66, 15, 133, 99, 153, 124, 67, 59, 35, 112, 156, 7, 71, 117, 4, 91, 41, 38, 152, 49, 130, 140, 86, 60, 146, 145, 143, 6, 18, 115, 9, 3, 22, 31, 46, 118, 136, 12, 68, 33, 103, 74, 21, 32, 111, 54, 104, 27, 85, 10, 23, 34, 25}

BOB = {111, 45, 134, 145, 27, 99, 14, 100, 74, 131, 138, 66, 146, 59, 118, 23, 8, 139, 129, 117, 31, 17, 25, 80, 153, 41, 38, 12, 130, 21, 6, 136, 91, 19, 22, 125, 46, 133, 90, 124, 152, 33, 10, 104, 29, 44, 122, 57, 128, 3, 85, 71, 54, 68, 105, 40, 32, 35, 67, 156, 34, 7, 143, 150, 18, 4, 151, 49, 103, 107, 88, 112, 115, 53, 60, 86, 98, 15, 140, 9}

INAYA = {90, 7, 103, 134, 143, 118, 49, 152, 6, 117, 128, 10, 60, 46, 17, 33, 105, 80, 122, 4, 125, 40, 107, 74, 145, 23, 57, 15, 41, 68, 140, 
86, 35, 138, 22, 66, 98, 19, 21, 153, 54, 115, 139, 25, 100, 29, 136, 8, 146, 111, 34, 133, 156, 91, 27, 129, 44, 9, 59, 124, 53, 12, 67, 112, 150, 18, 85, 88, 31, 151, 71, 14, 45, 3, 104, 38, 32, 131, 99, 130}
```

1. Créer `xuan`, `bob` et `inaya`, les `PlayList` des trois ami-es.
2. Créer la `PlayList` commune aux trois personnes.
3. Créer la `PlayList` des morceaux de Bob que ne possèdent pas ses deux amies.
