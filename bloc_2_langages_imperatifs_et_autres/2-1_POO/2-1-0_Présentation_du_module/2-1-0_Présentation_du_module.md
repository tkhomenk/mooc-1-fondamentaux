## Présentation du module: Programmation orientée objet

## Objectifs du module
Il s'agit d'aborder ici de façon pragmatique la programmation orientée objet à partir des aspects orientés objets du langage de programmation Python, puis d'un mini-projet réalisé en objet avec le module turtle de Python

## Prérequis
Savoir programmer en Python

Avoir les bases de la programmation impérative Python vous sont acquises. La matière correspond au contenu du MOOC "Apprendre à coder avec Python".

## Présentation des séquences / sommaire
Nous regardons les aspects orientés objets de Python. Nous présentons le vocabulaire de la Programmation orientée objet, la syntaxe Python pour les bases et quelques concepts plus avancés (comment réaliser l'encapsulation par exemple).

- Programmation orientée objet du point du vue du langage Python
- Modelisation orientée objet du projet du jeu "le Château"

## Enseignant

### Sébastien Hoarau

Maitre de Conférence en Informatique à l’Université de la Réunion et membre de l'IREM de la Réunion. Il enseigne l’initiation à la programmation impérative avec le langage Python3 à des 1ère années scientifiques. Initiation aux techno du web aussi : HTML, CSS, JavaScript
