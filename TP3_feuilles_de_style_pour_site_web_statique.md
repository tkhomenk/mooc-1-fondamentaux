# TP3 - Style et feuille de style pour page web statique

# L’attribut style

Le style d’un paragraphe peut être défini par l’attribut style, ajouté dans la balise ouvrante : 

* `<p style="background-color:tomato;">`
* `<h1 style="color:blue;">`
* `<body style="font-family:verdana;">`
* `<h2 style="font-size:300%;">`

# La feuille de style

Il est aussi possible de définir le style de la page web dans une feuille de style à part. Cette feuille de style (CSS : Cascading Style Sheets) peut alors être modifiée indépendamment du contenu et peut être réutilisée sur différentes pages du site.
Les feuilles de style permettent de faire de riches pages web avec des styles élaborés pour chaque division de la page. Le site w3school par exemple liste de nombreuses possibilités des css, illustrées par des exemples.
On donne 4 feuilles de style simples.
* Ouvrir les feuilles de style, les tester en ajoutant dans l’en-tête : `<link rel="stylesheet" href="style1.css">`, commenter rapidement ce qui les compose.
* Faire sa propre feuille de style et l’appliquer à son site.
* Cliquer droit sur un site web élaboré, afficher le code source et une des feuilles de style (un recherche de css donne habituellement le lien vers le ou les fichiers css utilisés par le site.)
