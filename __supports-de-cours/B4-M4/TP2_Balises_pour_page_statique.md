# TP2 - Quelques balises pour site web statique (60 mn)

## Balises de mise en forme

* Ajouter des commentaires avec la balise ouvrante fermante `< !-- Commentaire... -->`
* Ajouter un titre pour la page et un sous-titre par paragraphe avec les balises `<h1> </h1>` et `<h2> </h2>`
* Mettre une partie du texte en gras (bold) avec `<b> </b>` et une partie en italique avec `<i> </i>`
* Ajouter une liste ordonnée avec les balises `<ol> </ol>`. Les balise `<li> </li>` encadrent les éléments de la liste. Noter que les balises `<ul> </ul>` permettent de créer une liste sans numérotation.

## Balises pour l’insertion d’éléments multimédias

* Ajouter une image avec `<img “chemin_vers_image” alt = “texte à afficher si image ne s’affiche pas” >`
On peut remarquer que la balise est à la fois ouvrante et fermante, comme `<!DOCTYPE html>`

* Ajouter un son avec `<audio controls src="chemin_vers_son"> "texte à afficher si le son ne peut être lu" </audio>.`
Controls indique que le navigateur prend ses réglages par défaut pour la lecture du son.

* Ajouter une vidéo (au format ogg par exemple) avec 
	`<video controls width = "400"> <source src="chemin_vers_video" type="video/ogg"> “texte à afficher si la vidéo ne peut être lue” 	</video> `
Le logiciel vlc convertit les vidéos de tout type en vidéos au format ogg. Les format mp4 et WebM sont normalement acceptés par les navigateurs.

## Balises pour les liens hypertextes

Les liens hypertextes sont à l’origine du web, d’où le nom du protocole HTTP HyperText Transfer Protocol. Ils permettent de faire des liens entre des pages web.
* Insérer un lien hypertexte avec `<a href = “lien_vers_le_site_web”> texte du lien </a>`
* Ajouter un lien vers une adresse mail `<a href="mailto:anthony.juton@ens-paris-saclay.fr">Contact</a>`

