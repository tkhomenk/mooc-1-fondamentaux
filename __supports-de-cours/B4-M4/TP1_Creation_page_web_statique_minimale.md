# TP1 - Création d'une première page web minimaliste (30 mn)

L'objectif est simplement de créer une page web la simple possible.

Créer à l’aide d’un éditeur de texte (par exemple gedit sous Linux ou notepad++ sous Windows) une page web index.html statique avec le minimum :
* Le format : `<!DOCTYPE html>`
* Une balise ouvrante `<html lang= “fr”>` et une balise fermante `</html>`
* Un en-tête avec ses balises, un titre lui-même entouré de ses balises et l’usage des caractères utf-8 (qui permettent un meilleur respect des accents), :
`<head> <title> TP techno web </title> <meta charset="utf-8"> </head>`
* Un corps avec une ligne de texte : `<body> Une ligne de texte </body>`

![image-12.png](./image-12.png)

Il est possible d’ouvrir le fichier html directement sur le navigateur Firefox, via un double-clic ou un clic droit > Ouvrir avec une autre application si besoin.
Il est également possible de vérifier le respect de la norme et des bonnes pratiques sur le site w3c : https://validator.w3.org 
