# Introduction aux TPs technologies web

## Objectif

L'objectif des travaux pratiques de ce module est de mettre en oeuvre pou les comprendre les principales technologies web : HTML (et les feuilles de style css), PHP (et ses liens avec les bases de données SQL) et javascript.
Il ne s'agit pas de former des développeurs web, ce qui explique l'impasse faite sur les systèmes de gestion de contenu (Wordpress, Django, Flask...)
A la fin des TPs, l'apprenant sera capable de développer un site web utilisant quelques fonctionnalités de ces technologies et de le mettre en ligne.
L’apprenant intéressé pourra ensuite s’appuyer sur cette première introduction pour approfondir ce sujet à l’aide des nombreux tutoriels disponibles sur internet (openclassroom, w3schools...).

## Matériel utilisé

Le serveur web utilisé pendant les TPs est une machine virtuelle Linux Ubuntu 20.04.
C'est transposable très simplement sur une machine physique Linux moderne ou sur une Raspberry Pi (quelques mots seront ajoutés si besoin).
Les outils (Apache, MariaDB, PHP) fonctionnent également sur une machine Windows ou Mac et le support sur les forums est très important si besoin. Les sujets de TPs n'ont juste pas été testés sur ces systèmes.
La partie suivante n'est donc pas indispensable, mais recommandée pour suivre les TPs sans difficulté de configuration de logiciels.

## Installation d'une machine virtuelle Linux (45 mn)

La machine physique est nommée ici machine hôté et la machine virtuelle est parfois nommée VM (pour Virtual Machine).

### Création de la machine sur Virtual Box

On commence par créer une machine sur virtual box :
* Télécharger Virtual Box : https://www.virtualbox.org/ Cela fonctionne sur Windows, Mac et Linux.
* Installer et Exécuter Virtual Box.
* Cliquer sur Machine > Nouvelle... et choisir Type : Linux et Version : Ubuntu 64 bits.

![image.png](./image.png)

* Affecter au moins 2 Go de mémoire (cela dépend de la machine hôte, 4 Go c'est mieux).

![image-1.png](./image-1.png)

* Ajouter un disque dur virtuel, type VDI avec un stockage dynamiquement alloué, et une limite à 20 Go dans la mesure du possible.

![image-2.png](./image-2.png)
![image-3.png](./image-3.png)
![image-4.png](./image-4.png)
![image-5.png](./image-5.png)

* Cliquer sur Créer.

### Configuration de la machine

Pour ajouter 2 fonctionnalités à la machine virtuelle (VM pour virtual Machine), cliquer sur le bouton configuration : 
![image-6.png](./image-6.png)
* Dans l'onglet Général > Avancé, autoriser les copier/coller et le glisser/déposer de la machine hôte vers la machine virtuelle, c'est très pratique en développement.
![image-7.png](./image-7.png)
* Dans l'onglet Dossiers partagés, ajouter un dossier en indiquant le chemin sur la machine hôte et le chemin sur la machine destination /home/nom_utilisateur/dossierpartage par exemple.
![image-8.png](./image-8.png)

### Installation de Linux sur la machine

La machine étant créée, il faut lui installer un système d'exploitation. Pour cela, télécharger l'image du disque d'installation d'Ubuntu sur https://www.ubuntu-fr.org/download/.
* Démarrer la machine virtuelle créée ci-dessus

![image-11.png](./image-11.png)

* Dans la fenêtre de choix du disque de démarrage, ajouter, grâce à l'icône jaune à droite, le chemin vers le fichier iso téléchargé.

![image-10.png](./image-10.png)

* Suivre ensuite les instructions d'installation de Ubuntu, les propositions par défaut étant souvent bien adaptées. La machine virtuelle n'ayant accès qu'à son propre espace, il est normal, et sans risque, que l'installeur propose d'effacer l'ensemble du disque.

* Une fois l'installation terminée, redémarrer la machine et se logguer.

* Passer les fenêtres de démarrage. Autoriser les mises à jour lorsqu'elles sont proposées.

* Pour augmenter la résolution, cliquer droit sur le fond du bureau et choisir configuration d'affichage.

* Pour un bon fonctionnement du dossier partagé et du copier/coller entre machines hôte et virtuelle, ajouter les additions invités en cliquant Périphériques > Insérer l'image CD des additions invités... puis lancer et mettre son mot de passe à chaque que fois que c'est demandé. Redémarrer la machine ensuite et vérifier que le dossier partagé apparaît et que le copier/coller est possible.








