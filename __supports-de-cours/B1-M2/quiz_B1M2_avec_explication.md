1. Parmi les listes Python ci-dessous, laquelle est une liste en extension comportant 2 entiers, 1 chaîne de caractères et 3 booléens ?

    - `[-5, True, 10, True, False, 'mooc nsi']` (bonne réponse)
    - `[True] * 3 + [1, 2, 'hello']` 
    - `[0, 1, chaine, True, False, true]` 
    - `[True, '5', '8', False, False, hello]`

Pour ne pas se faire piéger, il faut bien comprendre que 

- les valeurs booléénnes `True` ou `False` doivent avoir une majuscule, sinon ce sont des indentificateurs de variable
- si un mombre est entre _quote_ comme '3.1416' il est considéré comme une chaîne de caractères
- on note aussi que l'expression `[True] * 3 + [1, 2, 'hello']` va bien générer trois booléens, deux entiers et une chaîne de caractères mais elle n'est pas en extension

2. Quelle(s) affirmation(s) ci-dessous sont _couramment_ admises lorsqu'on oppose _tableau_ (dans son acceptation algorithmique la plus stricte) des listes de Python ?

    - Le tableau est statique (bonne réponse)
    - Le tableau est non polymorphe (bonne réponse)
    - Le tableau se construit avec la fonction prédéfinie Python `Array`
    - Le tableau est non mutable

Rappelons qu'une liste _polymorphe_ signifie qu'elle contient des valeurs de différents types (par exemple des entiers et des booléens), tandis qu'une valeur _non mutable_ signifie qu'on ne peut changer sa valeur initiale, et on ne peut changer la longueur d'un tableau : il est _statique_. Dans cette distinction entre tableau et liste il y a à la fois des usages différents (on peut rallonger une liste), et des implémentations plus ou moins efficaces pour un usage ou un autre. 


3. Quelle est la version en extension de la compréhension de liste suivante : `[e for a, n in [(1, 3), ('hello', 2)] for e in [a] * n]`

    - `[1, 1, 1, 'hello', 'hello']` (bonne réponse)
    - `[[1, 1, 1], ['hello', 'hello']]`
    - `[1, 'hello', 1, 'hello', 1]`
    - Aucune : la compréhension provoque une erreur

La morale de cet exemple (et du suivant) est : les constructions en compréhension doivent rester simples pour rester... compréhensibles. Les doubles `for` sont donc rarement utilisés. La règle est la suivante, si les `for` sont au même niveau, on les traite de gauche à droite, comme sur notre exemple. Au premier passage de la première boucle le couple `(a, n)` va prendre la valeur `(1, 3)` ; dès lors la deuxième boucle faire parcourir à la variable `e`  la liste `[1, 1, 1]` et chacun de ces `1` va venir remplir notre liste construite en compréhension.

4. Quelle est la version en extension de la compréhension de liste suivante : `[[e for e in [a] * n] for a, n in [(1, 3), ('hello', 2)]]`

    - `[[1, 1, 1], ['hello', 'hello']]` (bonne réponse)
    - `[1, 1, 1, 'hello', 'hello']`
    - `[1, 'hello', 1, 'hello', 1]`
    - Aucune : la compréhension provoque une erreur

Cette fois le `for` le plus à gauche est aussi le plus imbriqué. On commence donc par le `for` externe, et pour le premier passage le couple `(a, n)` prend la valeur `(1, 3)` et alors le `for` interne produit la liste `[1, 1, 1]` qui est le premier élément de notre liste finale construite en compréhension.

5. On considère la fonction suivante :

    ```python
    def foo(tab):
        i, j = 0, len(tab)-1
        while i < j:
            if tab[i] == 1:
                tab[i], tab[j] = tab[j], tab[i]
                j -= 1
            if tab[i] == 0:
                i += 1
    ```

    Et le tableau `tab = [0, 1, 1, 0, 0, 1, 0]`. Que vaut `tab` après l'appel `foo(tab)` ?

    - `[0, 0, 0, 0, 1, 1, 1]` (bonne réponse)
    - `[0, 1, 0, 1, 0, 1, 0]` 
    - `[1, 1, 1, 0, 0, 0, 0]` 
    - inchangé : la fonction provoque une erreur

Suivons le code pas à pas :

 - au début i = 0 et j = length - 1 ou _length_ est la longueur du tableau : tab[i] est donc la valeur de gauche et tab[j] celle de droite
 - ensuite tant que i est strictement plus petit que j
   - si tab[i] vaut 1 on échange tab[i] et tab[j] et on décrémente j, donc on pousse la valeur de gauche à droite et on l'y laisse
   - sinon, si tab[i] vaut 0 on le laisse et passe à la valeur de gauche suivante

On voit alors que ce code pousse toutes les valeurs 1 à droite et laisse les valeurs 0 à gauche.

6. Dans quelles situations algorithmiques ci-dessous l'utilisation d'un tuple s'avère judicieux ?

    - Une fonction qui doit renvoyer plusieurs valeurs (bonne réponse)
    - Une affectation multiple (bonne réponse)
    - La modélisation d'une série de valeurs numériques qui varient au cours de l'exécution du programme
    - Le paramètre d'une fonction de tri en place

Le tuple permet de manipuler plusieurs items dans une seule variable mais de manière non mutable, sans pouvoir changer les valeurs une fois définies. Il est donc utile pour renvoyer plusieurs valeurs d'une fonction (faire un "paquet" en quelque sorte) ou affecter simultanément plusieurs valeurs, mais ne peut être utilisé pour des valeurs qui varient.

7. Quelle instruction ci-dessous ne permet pas à la variable `d` de référencer un dictionnaire ?

    - `d = {e**2 for e in range(10)}`  (bonne réponse)
    - `d = dict([('lundi', 'monday'), ('mardi', 'tuesday'), ('mercredi', 'wednesday')])`
    - `d = {'4': {'00': 'Requête erronée', '01': 'Authentification requise'},'5': {'00': 'Erreur interne du serveur'}}`
    - `a = {'ville':'Paris'} ; d = dict(a, monument = 'Tour Eiffel')` 

On voit qu'un dictionnaire se définit soit avec une construction de la forme '{ 'nom' : valeur ... }' soit avec la function `dict(...)` sous forme de liste de tuples à deux éléments ou en ajoutant des éléments à un autre dictionnaire, mais un simple liste de valeur n'est pas un dictionnaire : la première construction fourni un objet `set`. 

8. On considère une collection _C_ d'environ 300000 chaînes de caractères. Option 1 : on stocke ces mots comme clés d'un dictionnaire `D` auxquelles on associe une certaine information. Option 2 : on stocke les mots dans une liste `L`. 

    De plus, une autre liste `L2` contient environ 100000 chaînes de caractères. Quelles manipulations sont plus efficaces avec le dictionnaire ?

    - Tester l'appartenance des mots de `L2` à la collection _C_ ? (bonne réponse)
    - Supprimer un mot donné de la collection (bonne réponse)
    - Ajouter un mot à la collection (bonne réponse)
    - Ordonner les mots de la collection

Un dictionnaire est une table associative où des couples _clé : valeur_ sont stockés de manière à ce qu'on puisse y accéder rapidement par la clé (par exemple en utilisant une [fonction de hachage](https://fr.wikipedia.org/wiki/Fonction_de_hachage) ou des arbres binaires auto-équilibrés, dits [B-arbre](https://fr.wikipedia.org/wiki/Arbre_B)). Ici les chaînes de caractères sont les clés du dictionnaire. On peut donc efficacement tester l'appartenance d'une chaîne de caractères, ajouter ou supprimer un élément. En revanche, si le dictionnaire est basé sur une fonction de hachage (ce qui est le cas en Python) alors il est impossible d'ordonner les clés, ce qui serait le cas avec une implémentation avec un B-arbre.






