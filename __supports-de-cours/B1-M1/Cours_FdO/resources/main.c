//
//  main.c
//  essai-int
//
//  Created by Présentation on 29/09/14.
//  Copyright (c) 2014 Présentation. All rights reserved.
//

#include <stdio.h>
#include <math.h>

int main(int argc, const char * argv[])
{
    int i ;
    
    printf("Les entiers tiennent sur %ld octets\n\n", sizeof(int)) ;
    
    
    long int j = pow(2,8*sizeof(int))-1 ;
    i = j ;
    printf("2^32-1 = %d\n", i) ;
    
    
    i = 0b11111111111111111111111111111111 ;
    printf("0b11111111111111111111111111111111 = %d\n\n", i) ;
    
    j = pow(2,8*sizeof(int)-1) ;
    i = j ;
    printf("2^31 = %d\n", i) ;
    
    
    i = 0b10000000000000000000000000000000 ;
    printf("0b10000000000000000000000000000000 = %d\n", i) ;
    
   
    return 0 ;
    
}

