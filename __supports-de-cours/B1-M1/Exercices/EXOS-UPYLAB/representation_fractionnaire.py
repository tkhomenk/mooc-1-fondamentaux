def representation_fractionnaire(val, base=2, longueur=128):
    """
    Renvoit la représentation en base base de la valeur fractionnaire val
    hypothèse : 0 <= val < 1
    1 < base < 37
    les chiffres seront notés : '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    :param val: float (0 <= val < 1) valeur fractionnaire à représenter
    :param base: int (1 < base < 37) base
    :return: (str) '0.prefixe[(periodique)+|+]'
    en base base de la valeur fractionnaire val
    avec 0.prefixe periodique periodique ...
    avec prefixe <= longueur donnant la représentation en base base de val
    et 0.prefixe avec prefixe de longueur longueur,
    le préfixe de la représentation de val en base base :
    dans ce cas il n'y a pas de [(periodique)+]
    '0.0' si val vaut 0.0
    '0.prefixe+' dans le cas où l'on a atteint longueurs chiffres
    sans arriver à mettre val à 0 ni trouver un cycle
    exemple : avec  (0.1, 5) : '0.0(2)+' # représente 0.02222222...
    exemple : avec (0.1, 20) : '0.2' # en base 20
    exemple : (1/3, 10, 20) : '0.33333333333333330372+' # non terminé
    (dû aux erreurs dans la représentation de 1/3)
    """

    if val == 0.0:
        res = '0.0'
    else:
        CHIFFRES = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        prefixe = ''
        rencontre = []
        long = 0
        while long < longueur and val not in rencontre and val != 0:
            long += 1
            rencontre.append(val)
            val = val * base
            c = int(val)
            val = val - c
            prefixe += CHIFFRES[c]
        if val == 0:
            res =  '0.'+prefixe # pas de période
        elif val not in rencontre: # pas vu la période mais pas fini
            res =  '0.'+prefixe+'+' # pas de période
        else:
            #print('val : ', val, 'rencontre : ', rencontre)
            #print('prefixe : ', prefixe)
            i = rencontre.index(val)
            res =  '0.' + prefixe[:i] +'('+ prefixe[i:]+')+'
        #print('rencontre : ', rencontre)
    return res

print('rf(1/3, 3) :', representation_fractionnaire(1/3, 3))
print('rf(1/3, 10, 20) :', representation_fractionnaire(1/3, 10, 20))
print('rf(0.1, 2) :', representation_fractionnaire(0.1, 2))
print('rf(0.1, 5) :', representation_fractionnaire(0.1, 5))
print('rf(0.1, 20) :', representation_fractionnaire(0.1, 20))
print('rf(0.1, 33) :', representation_fractionnaire(0.1, 33))
print('rf(0.0, 33) :', representation_fractionnaire(0.0, 33))



