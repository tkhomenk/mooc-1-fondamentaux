CHIFFRES = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def valeur_chiffre(c, base):
    """renvoie la valeur du chiffre en base base
    param :c: (str) caractère donnant un chiffre 0-9 ou une lettre 'A'=10, ... 'Z' = 35
    return : valeur du chiffre c dans la base valeur_base"""

    assert c in CHIFFRES
    if '0' <= c and c <= '9':
        res = ord(c) - ord('0')
    else:
        res = 10 + ord(c) - ord('A')
    return res

def valeur_base_a(rep, a):
    """Valeur de ce qui est représenté par rep en base a
    param :rep : (str) représentation en base a
    param : a: (int) base
    return : valeur de rep (en base a)"""
    res = 0
    while len(rep) > 0:
        res = res * a + valeur_chiffre(rep[0], a)
        rep = rep[1:]
    return res

def representation_nombre_naturel_en_base_b(valeur, base):
    """donne en format str, la représentation d'un nombre valeur en base base
    valeur : valeur supérieure ou égale à 0,
    base : hypothèse 2 < base <= 37
    CHIFFRES[i] donne le chiffre de rang i (CHIFFRES[0] = 0, CHIFFRES[10] = 'A', ..."""
    res = ''
    assert valeur >= 0 and (2 <= base < 37)
    if valeur == 0:
        res = '0'
    else:
        while valeur > 0:
            valeur, i = divmod(valeur, base)
            res = CHIFFRES[i] + res
    return res

def changement_de_base(rep_a, a, b):
    """traduit la représentation d'une valeur entière positive
    en base a en une représentation en base b
    param :rep_a (str) représentation en base a de la valeur
    param :a: (int) base de rep_a avec 1 < a < 37
    param :b: (int) base de la représentation retournée
    return: représentation en base b de la valeur représentée par rep_a en base a"""

    print(valeur_base_a(rep_a, a))
    return representation_nombre_naturel_en_base_b(valeur_base_a(rep_a, a), b)


for d in (('A', 10, 2), ('A', 20, 8), ('101', 3, 10), ('101', 3, 16), ('0', 2, 16), ('14B', 20, 16), ('0', 36, 2), ('E', 36,2), ('143', 10, 36), ('5', 10, 2)):
    print('changement de base(',d,') =', changement_de_base(*d))

