title: INFO-F-102 – Fonctionnement des ordinateurs  
  Travaux pratiques – Séance 1

# Représentation binaire

<div class="exercice" markdown="1">

**Ex. 1**. *Convertissez les nombres suivants :*

-   *$10101_{2}$ en base 10,*

-   *$11111_{2}$ en base 10,*

-   *$72_{8}$ en base 10,*

-   *$4E_{16}$ en base 10,*

-   *$227_{10}$ en base 2 et 16,*

-   *$454_{10}$ en base 2 et 16,*

-   *$19_{10}$ en base 2, 8 et 16.*

</div>

<div class="exercice" markdown="1">

**Ex. 2**. *Combien de nombres binaires différents peut-on représenter
sur 3 bits ? sur 4 bits ? sur $n$ bits ? Justifiez.*

</div>

<div class="exercice" markdown="1">

**Ex. 3**. *Effectuez les opérations suivantes en utilisant les
opérations euclidiennes.*

-   *$1011_2 + 1110_2$,*

-   *$1A7_{16} + 909_{16}$,*

-   *$433_{8} - 136_{8}$.*

</div>

<div class="exercice" markdown="1">

**Ex. 4**. *Effectuez les opérations suivantes (où $a \bmod b$ dénote le
reste de la division de $a$ par $b$, $a\div b$ denote la division
entière de $a$ par $b$, et $a>>n$ représente le décalage de $n$
positions vers la droite du nombre $a$). Donnez tous les résultats sur 8
bits. Exprimez ensuite le résultat des opérations $\div$ et $\mod$ en
terme de décalage et/ou de masque.*

-   *$0101\ 1101 \wedge 0011\ 1100$,*

-   *$0101\ 1101 \vee 0011\ 1100$,*

-   *$0101\ 1101 b\ensuremath{\operatorname{\mathrm{XOR}}}0011\ 1100$,*

-   *$\neg 0101\ 1101$,*

-   *$0101\ 1101 b\ensuremath{\operatorname{\mathrm{XOR}}}1111\ 1111$*

-   *$0011\ 0011_2 >> 3_{10}$*

-   *$0010\ 1101_2 \div 100_2$,*

-   *$0010\ 1101_2 \bmod 100_2$,*

</div>

# Nombres négatifs

<div class="exercice" markdown="1">

**Ex. 5**. *On considère une représentation des nombres signés sur 8
bits. Donnez la représentation décimale de:*

-   *$0000\ 1101_2$, en considérant qu’on a utilisé le bit de signe;*

-   *$1100\ 0000_2$, en considérant qu’on a utilisé le bit de signe;*

-   *$1011\ 1101_2$, en considérant qu’on a utilisé le complément à 1;*

-   *$0111\ 1001_2$, en considérant qu’on a utilisé le complément à 1;*

-   *$0001\ 1001_2$, en considérant qu’on a utilisé le complément à 2;*

-   *$1100\ 1101_{2}$, en considérant qu’on a utilisé le complément à
    2;*

-   *$0011\ 0011_{2}$, en considérant qu’on a utilisé l’excès à 128; et
    de*

-   *$1000\ 1010_2$, en considérant qu’on a utilisé l’excès à 128.*

</div>

<div class="exercice" markdown="1">

**Ex. 6**. *Représentez, en binaire, sur $8$ bits, le nombre $-78_{10}$,
en utilisant les 4 encodages vus au cours, à savoir:*

1.  *avec bit de signe*

2.  *en complément à 1*

3.  *en complément à 2*

4.  *en excès à 128.*

</div>

<div class="exercice" markdown="1">

**Ex. 7**. *On considère une représentation des nombres signés sur 16
bits, en utilisant le complément à 2. Donnez la représentation en
binaire de :*

-   *$+ 1032_{10}$.*

-   *$- 721_{10}$.*

</div>

<div class="exercicestar" markdown="1">

**$\star$ Ex. 8**. *On considère une représentation sur 8 bits. Pour les
quatre représentations des nombres négatifs vus au cours (bit de signe,
complément à 1, complement à 2, excès à 128), déterminez quel est le
plus petit et le plus grand nombre représentable.*

</div>

# Virgule flottante

<div class="exercice" markdown="1">

**Ex. 9**. *Exprimez les valeurs suivantes en base 2:*

-   *$12{,}5_{10}$*

-   *$12{,}125_{10}$*

-   *$12{,}625_{10}$*

-   *$1432{,}45_{10}$*

</div>

<div class="exercice" markdown="1">

**Ex. 10**. *On considère une représentation des nombres flottants selon
la norme IEEE 754 simple précision. Quelle est la valeur décimale des
représentations internes suivantes ?*

-   *$C6570000_{16}$,*

-   *$42EF9100_{16}$.*

</div>

<div class="exercice" markdown="1">

**Ex. 11**. *On considère une représentation des nombres flottants selon
la norme IEEE 754 simple précision. Quelle est la représentation interne
des nombres suivants ? Exprimez les résultat final en base 16.*

-   *$+ 1432,45_{10}$,*

-   *$- 721,25_{10}$.*

</div>

<div class="exercicestar" markdown="1">

**$\star$ Ex. 12**. *La norme IEEE 754 admet deux nombres spéciaux, à
savoir $\textbf{NaN}$(pour *not a number*: utilisé en général en cas de
division par 0) et $\infty$[^1]. Pour savoir quel est le résultat d’une
opération dont une des deux opérandes est $\textbf{NaN}$ ou $\infty$, on
a recours à un tableau, par exemple pour la division :*

<div class="center" markdown="1">

|                   *$x/y$*                    | *$y\neq 0,\infty,\ensuremath{\textbf{NaN}}$* | *$y=\infty$* |  *$y=0$*   | *$y=\textbf{NaN}$* |
|:--------------------------------------------:|:--------------------------------------------:|:------------:|:----------:|:------------------:|
| *$x\neq 0,\infty,\ensuremath{\textbf{NaN}}$* |       *valeur la plus proche de $x/y$*       |     *0*      | *$\infty$* |     ***NaN***      |
|                 *$x=\infty$*                 |                  *$\infty$*                  |  ***NaN***   | *$\infty$* |     ***NaN***      |
|                   *$x=0$*                    |                    *$0$*                     |    *$0$*     | ***NaN***  |     ***NaN***      |
|              *$x=\textbf{NaN}$*              |                  ***NaN***                   |  ***NaN***   | ***NaN***  |     ***NaN***      |

</div>

*On vous demande d’établir un tableau similaire pour la multiplication.*

</div>

<div class="exercicestar" markdown="1">

**$\star$ Ex. 13**. *Démontrez que, à part $\infty$ et
$\ensuremath{\textbf{NaN}}$, seuls des nombres rationnels peuvent être
représentés à l’aide de la norme IEEE 754.*

</div>

# Aide-mémoire : représentation des nombres négatifs

En supposant une représentation sur $m$ bits:

-   **Bit de signe**: Un nombre binaire négatif $n^-$ sur $m-1$ bits est
    représenté par $1n^-$; un nombre positif $n^+$ sur $m-1$ bits est
    représenté par $0n^+$ (=représentation standard).

-   **Complément à 1**: Un nombre positif $n^+$ sur $m-1$ bits est
    représenté par $0n^+$ (=représentation standard), un nombre négatif
    $n^-$ sur $m-1$ bits est représenté par $1\overline{n^-}$, où
    $\overline{n^-}$ est obtenu en inversant tous les bits de $n^-$.

-   **Complément à 2**: Un nombre positif $n^+$ sur $m-1$ bits est
    représenté par $0n^+$ (=représentation standard), un nombre négatif
    $n^-$ est représenté par la représentation binaire du complément à
    deux de sa valeur absolue, à savoir $2^{m}-|n^-|$.

-   **Excès à $x$**: Un nombre $n$, qu’il soit positif ou négatif est
    représenté par la représentation binaire de $n+x$. On préférera
    choisir $x$ comme une puissance de $2$, en général $2^{m-1}$.

# Aide-mémoire : IEEE 754 

En 32 bits (simple précision):

<div class="center" markdown="1">

| 1 bit signe $s$ | 8 bits exposant $e$ | 23 bits de signifiant $f$ |
|:---------------:|:-------------------:|:-------------------------:|

</div>

1.  Le bit de signe est à 1 ssi le nombre est négatif.

2.  L’exposant est stocké en excès à 127 (il peut être négatif).

3.  Le signifiant représente la partie décimale du nombre. Cette partie
    est toujours normalisée: on adapte l’exposant pour qu’elle soit de
    la forme $1.f$, et on ne représente que la partie $f$.

Pour $\infty$ et $\ensuremath{\textbf{NaN}}$:

-   $\infty$ est représenté par $e=1\cdots 1$ et $f=0\cdots
      0$. (il existe donc un infini positif et un infini négatif)

-   **NaN** est représenté par $e=1\cdots 1$ et $f\neq
      0\cdots 0$ (existe donc aussi en positif et négatif).

[^1]: *Remarque: en pratique, il existe un infini positif et un infini
    négatif. Nous ne considérerons qu’un seul infini dans ces exercices,
    par souci de simplification.*
