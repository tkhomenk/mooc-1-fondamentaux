1. En rapport avec la question 1 du TP : donner la liste des descripteurs de notre table, dans l'ordre :

    réponse (un champ texte) : sexe preusuel annais nombre

2. En rapport avec la question 1 du TP : quelle est la longueur de la liste ` TABLE_PRENOMS` ?

    réponse : 667364

3. En rapport avec la question 3 du TP : combien de fois le prénom Emmanuel a-t-il été donné en 1977 ?

    réponse : 3469

4. En rapport avec la question 4 du TP : si on ne compte pas les prénoms rares, quel a été le prénom féminin le plus donné en 2000 ?

    réponse : Léa

5. En rapport avec la question 5 du TP : quelle année les prénoms rares ont-ils été les moins utilisés ?

    réponse : 1916

6. En rapport avec la question 6 du TP : entre 1900 et 2019, combien de fois a-t-on attribué le prénom Marie ?

    réponse : 2259135
