## Une introduction aux 3 types construits. Les tableaux

Cette vidéo introduit le Module 2 qui va présenter les trois types construits que sont :
- le tableau,
- le p-uplet,
- le dictionnaire
puis enchaine par la première structure : le tableau.

**Pour chacune des structures, vous avez deux entrées possibles et complémentaires :**
- une vidéo présentant l'essentiel des manipulations sur la structure
- un texte pour approfondir

_L'ordre dans lequel vous abordez les supports n'a pas vraiment d'importance._

[![Vidéo 1 B1-M2-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M2-S1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M2-S1.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
