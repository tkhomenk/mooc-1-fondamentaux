Avant de pouvoir étudier la manière dont l’ordinateur *traite*
l’information, nous devons fixer la manière dont cette information est
représentée. L’objet de ces leçons est d’étudier la représentation
*binaire*, qui est aujourd’hui[^1] la représentation universellement
utilisée en informatique pour toutes les informations que les
ordinateurs manipulent.

**Définition :**

La représentation *binaire* signifie que toute l’information sera
représentée à l’aide de **deux symboles uniquement**. Ces deux symboles
sont en général les chiffres $0$ et $1$, mais on peut également
considérer que ce sont les valeurs logiques *faux* et *vrai*. 

On peut considérer que utiliser deux valeurs permet de définir un "atome" d'information (une seule valeur ne distingue rien). 

$\square$

Dans ce chapitre, nous développerons des techniques permettant de
représenter tout type d’information (nombres, texte, images,…) à l’aide
d’un représentation binaire, et de manipuler cette représentation. Nous
verrons également comment concevoir des techniques de détection et de
correction d’erreur sur cette représentation. Mais avant d’aborder cette
matière, il est important de bien comprendre la différence entre une
information, et sa représentation. L’exemple suivant illustre cette
différence.

**Exemple :**

Considérons la quantité 17, nous pouvons en trouver plusieurs
représentations:

-   la représentation usuelle, dite en base $10$: 17,

-   en base $2$: 10001 (nous aborderons les les notions de base et de
    changement de base en détail dans la suite),

-   en chiffres romains: XVII,

-   …

$\blacksquare$

On voit donc qu’une même information, qu’un même concept admet plusieurs
représentations. Le choix de la représentation binaire comme
représentation universelle en informatique s’explique par le fait qu’on
peut aisément représenter deux valeurs différentes à l’aide d’états
différents de composants électriques comme les transistors. Ces même
transistors, assemblés en *portes logiques*, permettent également de
manipuler l’information binaire.

<div id="refs" class="references csl-bib-body hanging-indent"
markdown="1">

<div id="ref-Setun" class="csl-entry" markdown="1">

Brousentsov, N. P., Maslov S. P., Ramil Alvarez J., and Zhogolev E. A.
n.d. “Development of Ternary Computers at Moscow State University.”
Accessed August 8, 2018.
<http://www.computer-museum.ru/english/setun.htm>.

</div>

</div>

[^1]: Il y a eu quelques expériences utilisant des représentations
    *ternaires*. On peut citer les ordinateurs soviétiques Setun
    (1959–1965) (Brousentsov et al. n.d.).
