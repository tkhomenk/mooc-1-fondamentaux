## Atelier: étude du cas "Zoo": le schéma normalisé


Nous reprenons l'étude du schéma **"Zoo"** vu dans le chapitre ["Le modèle relationnel" Atelier : une étude de cas](https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_1_repr_donnees_information/1-4_bases_de_donnees/1-4-2_Modele_relationnel/1-4-2-4-atelierQ1.html). Maintenant, on va chercher un schéma correct.

> -   Donner une décomposition en troisième forme normale.
>
> -   Quelles sont, dans la décomposition, les clés primaires et
>     étrangères ?
>
> -   Quel serait le schéma E/A correspondant?
>
> -   Est-il encore possible d'avoir les anomalies constatées dans la
>     table initiale ?
>
> -   Pour conclure, vous pouvez (optionnel) installer un outil de
>     conception comme le *MySql workbench*
>     (<https://www.mysql.com/fr/products/workbench/>) et saisir votre
>     schéma entité association avec le module *Entity relationship
>     diagram* (Fig. 45).
>     À partir de là vous pouvez tout paramétrer et engendrer
>     automatiquement le schéma de la base.
>
>     [![figure 45](mysqlworkbench.png)](mysqlworkbench.png)
>
>     Fig 45. L'outil de conception de MySQL
