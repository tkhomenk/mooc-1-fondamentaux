## Une étude de cas 1/5 : expression des besoins, conception

Voici une étude de cas qui va nous permettre de récapituler à peu près
tout le contenu de ce cours. Nous étudions la mise en œuvre d´une base
de données destinée à soutenir une application de messagerie
(extrêmement simplifiée bien entendu). Même réduite aux fonctionnalités
de base, cette étude mobilise une bonne partie des connaissances que
vous devriez avoir assimilées. Vous pouvez vous contenter de lire le
chapitre pour vérifier votre compréhension. Il est sans doute également
profitable d´essayer d´appliquer les commandes et scripts présentés.

## Une étude de cas

Imaginons donc que l´on nous demande de concevoir et d´implanter un
système de messagerie, à intégrer par exemple dans une application web
ou mobile, afin de permettre aux utilisateurs de communiquer entre eux.
Nous allons suivre la démarche complète consistant à analyser le besoin,
à en déduire un schéma de données adapté, à alimenter et interroger la
base, et enfin à réaliser quelques programmes en nous posant, au
passage, quelques questions relatives aux aspects transactionnels ou aux
problèmes d´ingénierie posés par la réalisation d´applications liées à
une base de données.

## Expression des besoins, conception

> **Supports complémentaires**

-   [Diapositives: conception](http://sql.bdpedia.fr/files/slcas-conception.pdf)
-   [Vidéo sur la conception](https://mediaserver.cnam.fr/videos/messagerie-conception/)


Dans un premier temps, il faut toujours essayer de clarifier les
besoins. Dans la vie réelle, cela implique beaucoup de réunions, et
d´allers-retours entre la rédaction de documents de spécification et la
confrontation de ces spécifications aux réactions des futurs
utilisateurs. La mise en place d´une base de données est une entreprise
délicate car elle engage à long terme. Les tables d´une base sont comme
les fondations d´une maison: il est difficile de les remettre en cause
une fois que tout est en place, sans avoir à revoir du même coup tous
les programmes et toutes les interfaces qui accèdent à la base.

Voici quelques exemples de besoins, exprimés de la manière la plus
claire possible, et orientés vers les aspects-clé de la conception
(notamment la détermination des entités, de leurs liens et des
cardinalités de participation).

 -   "Je veux qu´un utilisateur puisse envoyer un message à un
     autre"
 -   "Je veux qu´il puisse envoyer à *plusieurs* autres"
 -   "Je veux savoir qui a envoyé, qui a reçu, quel message"
 -   "Je veux pouvoir répondre à un message en le citant"


Ce n´est que le début. On nous demandera sans doute de pouvoir envoyer
des fichiers, de pouvoir choisir le mode d´envoi d´un message
(destinataire principal, copie, copie cachée, etc.), de formatter le
message ou pas, etc

On va s´en tenir là, et commencer à élaborer un schéma
entité-association. En première approche, on obtient celui de la
Fig.55.

<img src="ea-messagerie-1.png" alt="figure 55" width="500px">

> *Fig. 55.* Le schéma de notre messagerie, première approche

Il faut *nommer* les entités, définir leur *identifiant* et les
*cardinalités* des associations. Ici, nous avons une première ébauche
qui semble raisonnable. Nous représentons des entités qui émettent des
messages. On aurait pu nommer ces entités "Personne" mais cela aurait
semblé exclure la possibilité de laisser une *application* envoyer des
messages (c´est le genre de point à clarifier lors de la prochaine
réunion). On a donc choisi d´utiliser le terme plus neutre de
"Contact".

Même si ces aspects terminologiques peuvent sembler mineurs, ils
impactent la compréhension du schéma et peuvent donc mener à des
malentendus. Il est donc important d´être le plus précis possible.

Le schéma montre qu´un contact peut envoyer plusieurs messages, mais
qu´un message n´est envoyé que par un seul contact. Il manque sans
doute les destinataires du message. On les ajoute donc dans le schéma de
la Fig. 56.

<img src="ea-messagerie-2.png" alt="figure 56" width="500px">

> *Fig. 56.* Le schéma de notre messagerie, avec les destinataires

Ici, on a considéré qu´un message peut être envoyé à plusieurs contacts
(cela fait effectivement partie des besoins exprimés, voir ci-dessus).
Un contact peut évidemment recevoir plusieurs messages. Nous avons donc
une première association plusieurs-plusieurs. On pourrait la réifier en
une entité nommée, par exemple "Envoi". On pourrait aussi qualifier
l´association avec des attributs propres: le mode d´envoi par exemple
serait à placer comme caractéristique de l´association, et pas du
message car un même message peut être envoyé dans des modes différents
en fonction du destinataire. Une des attributs possible de
l´association est d´ailleurs la date d´envoi: actuellement elle
qualifie le message, ce qui implique qu´un message est envoyé *à la
même date* à tous les destinataires. C´est peut-être (sans doute) trop
restrictif.

On voit que, même sur un cas aussi simple, la conception impose de se
poser beaucoup de questions. Il faut y répondre en connaissance de
cause: la conception, c´est un ensemble de choix qui doivent être
explicites et informés.

Il nous reste à prendre en compte le fait que l´on puisse répondre à un
message. On a choisi de représenter de manière générale le fait qu´un
message peut être le successeur d´un autre, ce qui a l´avantage de
permettre la gestion du cas des renvois et des transferts. On obtient le
schéma de la Fig. 57, avec une association réflexive sur les messages.



<img src="ea-messagerie.png" alt="figure 57" width="600px">

>  *Fig. 57.* Le schéma complet de notre messagerie

Un schéma peut donc avoir plusieurs successeurs (on peut y répondre
plusieurs fois) mais un seul prédécesseur (on ne répond qu´à un seul
message). On va s´en tenir là pour notre étude.

À ce stade il n´est pas inutile d´essayer de construire un exemple des
données que nous allons pouvoir représenter avec cette modélisation (une
"instance" du modèle). C´est ce que montre par exemple la *Fig. 58.*.

[![figure58 ](instance-messagerie.png)](instance-messagerie.png)

>  *Fig. 58.* Une instance (petite mais représentative) de notre messagerie

Sur cet exemple nous avons quatre contacts et quatre messages. Tous les
cas envisagés sont représentés:

 -   un contact peut émettre plusieurs messages (c´est le cas pour
     Serge ou Philippe)
 -   un contact peut aussi recevoir plusieurs messages (cas de Sophie)
 -   un message peut être envoyé à plusieurs destinataires (cas du
     message 4, "Serge a dit\...", transmis à Sophie et Cécile)
 -   un message peut être le successeur d´un (unique) autre (messages
     2, 3, 4) ou non (message 1)
 -   un message peut avoir plusieurs successeurs (message 1) mais
     toujours un seul prédécesseur.


Prenez le temps de bien comprendre comment les propriétés du modèle sont
représentées sur l´instance.

Nous en restons là pour notre étude. Cela n´exclut en aucun cas
d´étendre le modèle par la suite (c´est inévitable, car des besoins
complémentaires arrivent toujours). Il est facile

 -   d´ajouter des attributs aux entités ou aux associations existantes;
 -   d´ajouter de nouvelles entités ou associations.

En revanche, il est difficile de revenir sur les choix relatifs aux
entités ou aux associations déjà définies. C´est une très bonne raison
pour faire appel à toutes les personnes concernées, et leur faire
valider les choix effectués (qui doivent être présentés de manière
franche et complète).


