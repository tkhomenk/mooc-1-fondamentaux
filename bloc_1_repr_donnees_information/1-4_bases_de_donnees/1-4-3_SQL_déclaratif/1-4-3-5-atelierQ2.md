Pour les requêtes suivantes, en revanche, vous avez droit à
l'imbrication (il serait difficile de faire autrement).

> -   Nom des voyageurs qui ne sont pas allés en Corse
> -   Noms des voyageurs qui ne vont qu'en Corse s'ils vont quelque
>     part.
> -   Nom des voyageurs qui ne sont allés nulle part
> -   Les logements où personne n'est allé
> -   Les voyageurs qui n'ont jamais eu l'occasion de faire de la
>     plongée

Vous pouvez finalement reprendre quelques-unes des requêtes précédentes
et les exprimer avec l'imbrication.


