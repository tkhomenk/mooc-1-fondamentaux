## Atelier: requêtes SQL sur la base Voyageurs 


Sur notre base des voyageurs en ligne (ou sur la vôtre, après
installation d'un SGBD et chargement de nos scripts), vous devez
exprimer les requêtes suivantes:

> -   Nom des villes
> -   Nom des logements en Bretagne
> -   Nom des logements dont la capacité est inférieure à 20
> -   Description des activités de plongée
> -   Nom des logements avec piscine
> -   Nom des logements sans piscine
> -   Nom des voyageurs qui sont allés en Corse
> -   Les voyageurs qui sont allés ailleurs qu\'en Corse
> -   Nom des logements visités par un auvergnat
> -   Nom des logements et des voyageurs situés dans la même région
> -   Les paires de voyageurs (donner les noms) qui ont séjourné dans le
>     même logement
> -   Les voyageurs qui sont allés (au moins) deux fois dans le même
>     logement
> -   Les logements qui ont reçu (au moins) deux voyageurs différents
> -   ...

**Contrainte**: n'utilisez pas l'imbrication, pour aucune requête (et
forcez-vous à utiliser la forme déclarative, même si vous connaissez
d'autres options que nous étudierons dans le prochain chapitre).

