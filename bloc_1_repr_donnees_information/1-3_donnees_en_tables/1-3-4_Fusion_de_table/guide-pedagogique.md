Dans ces trois sections : Fusion de Tables, Sauvegarde des données et Jointure,  

- comme précédemment pour le tri des données on vous invite à suivre cette vidéo, tout en manipulant vous-même sur l'interface de façon à acquérir à la fois les savoir et les savoir-faire sur aspects du sujet ; ainsi l'exercice pratique associé à ce stade est de reproduire puis essayer des variantes ou tenter des extensions des fonctions proposées ; en cas de doute ou de besoin de précisions, ne pas hésiter à [questionner le forum](https://mooc-forums.inria.fr/moocnsi/c/representation-des-donnees/a-propos-du-mooc/201) ;

- de plus, la notion de données en table est inhérente à celle de base de données relationelles qui utilisent largement des tables et qu'on abordera à la section suivante, 1.4 ; ici nous travaillons sur le "coeur" de ces représentations pour soulever un peu le "capot du moteur" et se préparer aux concepts plus complexes qui vont suivre.



