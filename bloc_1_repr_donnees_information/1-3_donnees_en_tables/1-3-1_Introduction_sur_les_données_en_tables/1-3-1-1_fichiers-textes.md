## Les _fichiers textes_



### De la feuille de tableur au format CSV

Le tableur est un outil bureautique qui permet de manipuler des données rangées dans des tableaux. En général, les documents ainsi créés ne sont pas visualisables et encore moins éditables via un simple éditeur de texte. Voici le message obtenu lorsqu'on tente d'ouvrir un fichier au format `.ods` avec l'éditeur Visual Studio Code :

> The file is not displayed in the editor because it is either binary or uses an unsupported text encoding. Do you want to open it anyway?

Et si on insiste :

![feuille ods avec VSCode](images/texte_ods.png)

Voici le fichier original ouvert dans LibreOffice :

![feuille ods avec LibreOffice](images/fruits_ods.png)




Or, ce sont ces _fichiers textes_ que nous pouvons manipuler via un programme Python.

Le format CSV (_Coma Separated Values_) est un format texte où chaque ligne du fichier est un ensemble de données séparées par des vrigules. Les deux fichiers exemples [`countries.csv`](tables/countries.csv) et [`cities.csv`](tables/cities.csv) sont de tels fichiers. On peut ouvrir ces fichiers avec un tableur. Ci-dessous les premières lignes de `countries.csv` ouvert avec LibreOffice :

![capture LibreOffice](images/tableur_countries.png)

Et voici le même fichier ouvert dans un éditeur de texte :

![capture VSCode](images/texte_countries.png)

Avec un tableur, il est possible de créer des fichiers CSV, en choisissant même le séparateur qui peut être autre chose qu'une virgule. En effet pour des données françaises où le séparateur décimal est la virgule justement, il est souvent préférable d'utiliser un autre caractère : le point virgule ou la tabulation (donnant alors le format `.tsv` pour _Tab-Sperated Values_).


### Manipuler un fichier texte avec Python

Si vous n'êtes pas complètement débutant en programmation Python, vous savez probablement ouvrir et manipuler un fichier texte dans un script Python :

```python
with open(mon_fichier, 'r', encoding=mon_encodage) as fichier_en_lecture:
    # traitement sur fichier_en_lecture
```

Avec les fichiers CSV (le terme CSV est utilisé de façon générique pour tous les fichiers de données en table, même si le séparateur n'est pas la virgule), ce que nous souhaitons c'est :

1. ouvrir le fichier
2. récupérer les données qui s'y trouvent et les stocker dans une structure Python pour manipulations diverses

### `list` de `tuple` (ou de `list`) 

Une première possibilité qui exploite nos connaissances de Python à savoir :

- les fichiers textes
- les types construits

va permettre de stocker les données dans une liste de tuples (ou de listes si on pense devoir modifier les valeurs de nos données). Ci-dessous une fonction possible pour la lecture d'un fichier CSV et la récupération de ces données dans une liste de tuples. Le fichier ne contient donc pas de descripteurs :

```python
def lecture(fichier, delimiter, encodage='utf-8'):
    liste_donnees = []
    with open(fichier, 'r', encoding=encodage) as entree:
        for donnee in entree:
            liste_donnees.append(tuple(donnee.strip().split(delimiter)))
    return liste_donnees
```

_Rappel_ : l'objet fichier de Python offre un itérable sur les chaînes de caractères que constitue chacune des lignes du fichier. Une simple boucle `for` permet donc de toutes les parcourir. 

Appliquons cette fonction à notre fichier de pays :

```python
l_pays = lecture('tables/countries.csv', delimiter=';')
```

On obtient bien une liste de tuples :

```python
>>> l_pays[16]
('AZ', 'Azerbaijan', 'Baku', '86600', '9942334', 'AS')
``` 

Dans cet exemple, il est probable que nous n'ayons pas à modifier les données (nous n'allons pas redéfinir la population de l'Azerbaïjan) et donc un tuple convient.

Il est dommage de perdre la signification des données, ie les descripteurs. On peut utiliser un dictionnaire à la place du tuple.

### `list` et dictionnaires

Voici une version modifiée de la précédente fonction `lecture` pour enregistrer les données dans des dictionnaires :

```python
def lecture(fichier, delimiter, encodage='utf-8'):
    liste_donnees = []
    with open(fichier, 'r', encoding=encodage) as entree:
        labels = entree.readline().strip().split(delimiter)
        for donnee in entree:
            liste_des_infos = donnee.strip().split(delimiter)
            d = {labels[index]:info for index, info in enumerate(liste_des_infos)}
            liste_donnees.append(d)
    return liste_donnees
```

_Rappel_ : la méthode `readline()` permet de récupérer la chaîne de caractères de la ligne courante de notre itérable, et de faire avancer d'une ligne l'itérable en question. Ici, grâce à cette méthode, nous récupérons la première ligne, différente des autres.

Et cette fois :

```python
>>> l_pays[16]
{'code': 'AZ',
 'nom': 'Azerbaijan',
 'capitale': 'Baku',
 'aire': '86600',
 'pop': '9942334',
 'continent': 'AS'}
```

Dans la suite, nous supposerons avoir un tableau de dictionnaires, c'est le plus souvent comme cela que nous manipulerons avec Python des données en table. Les tuples seront utilisés lorsque le fichier CSV ne fournit pas de descripteurs. Nous verrons aussi deux modules :

- `csv` qui facilite la lecture du fichier et la récupération des données dans une structure
- `pandas` qui offre toute une syntaxe pour la récupération **et** la manipulation des données
