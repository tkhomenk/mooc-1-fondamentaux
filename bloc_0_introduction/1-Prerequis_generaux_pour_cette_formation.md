### Quelles sont les formations préliminaires à cette formation "lourde" ?

Il faut connaître les concepts de base et s’initier à la programmation. En bref on invite à [s'initier à l'enseignement en Sciences Numériques et Technologie (SNT)](https://www.fun-mooc.fr/fr/cours/sinitier-a-lenseignement-en-sciences-numeriques-et-technologie) et s'initier à la programmation Python. Il est aussi recommander d'approfondir sa culture scientifique du numérique.

#### Acquérir une culture scientifique et technique de base en informatique

- En allant https://classcode.fr/snt => _section S_ vous aurez les premières bases, le vocabulaire, les idées introductives, un moyen aussi de vérifier que cela est fait pour vous ;)

- https://classcode.fr/snt => _section S+_ permet d'aborder des grands sujets du numériques à la fois techniques et sociétaux pour que les choses fassent du sens au delà de la formation disciplinaire à l'informatique.

- https://classcode.fr/snt => _section N_ correspond aux thématiques de l'enseignement des _sciences numériques et technologie_ [SNT](https://fr.wikipedia.org/wiki/Sciences_num%C3%A9riques_et_technologie), et permet de mieux connaître ces sujets.

Note : Il faudra aussi disposer de ressources sur les métiers et secteurs industriels du numérique, c'est en cours.

Note : There is also an [English version of these resources](https://pixees.fr/classcode-v2/classcode-informatics-and-digital-creation-online-free-open-course/) if of any help, including digital culture skills acquisition.

#### S’initier à la programmation Python :

Nous proposons trois solutions possibles, selon qu'on enseigne au niveau élémentaire de découverte de la programmation ou à un niveau plus avancé :

- Introduction minimale pour SNT : 
    - https://classcode.fr/snt => _section T_ sélectionne les bases pour commencer à programmer.
- Formation minimale pour NSI : 
    - [Apprendre à coder avec Python](https://www.fun-mooc.fr/courses/course-v1:ulb+44013+session04/about)
- Formation avancée pour NSI : 
    - [Python 3 : des fondamentaux aux concepts avancés du langage](https://www.fun-mooc.fr/courses/course-v1:UCA+107001+session02/about)

Bien entendu ce n'est pas exclusif, toute bonne formation à la programmation Python sera parfaite.
