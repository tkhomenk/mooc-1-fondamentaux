## Preuve d'algorithme

1. Analyse d'algorithme
2. Complexité d'un algorithme : calcul de la complexité
3. Complexité d'un algorithme : ordres de grandeur
4. **Preuve d'algorithme**

[![Vidéo 4 part 1 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video2a.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video2a.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 4 part 2 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video2b.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video2b.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 4 part 3 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video2c.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video2c.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
