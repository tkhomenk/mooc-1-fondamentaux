## Analyse d'algorithme: Introduction à la séquence

1. **Analyse d'algorithme**
2. Complexité d'un algorithme : calcul de la complexité
3. Complexité d'un algorithme : ordres de grandeur
4. Preuve d'algorithme

Dans cette séquence, à chaque étape de notre exposé, nous utiliserons le fil rouge du calcul de la complexité pour illustrer nos propos, à l'aide d'exercices et de démonstrations.

[![Vidéo 1 part 1 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1a.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1a.mp4)

## Transcription de la vidéo 

(en cours de mise en place)


## Analyse d'algorithme: Schéma algorithmique

[![Vidéo 1 part 2 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1b.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1b.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
