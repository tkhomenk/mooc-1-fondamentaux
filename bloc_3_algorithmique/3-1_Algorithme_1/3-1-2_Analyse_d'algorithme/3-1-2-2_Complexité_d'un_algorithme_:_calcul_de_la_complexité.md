## Complexité d'un algorithme : calcul de la complexité

1. Analyse d'algorithme
2. **Complexité d'un algorithme : calcul de la complexité**
3. Complexité d'un algorithme : ordres de grandeur
4. Preuve d'algorithme

[![Vidéo 2 part 1 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1c.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1c.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 2 part 2 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1d.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1d.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
