## Complexité d'un algorithme : ordres de grandeur

1. Analyse d'algorithme
2. Complexité d'un algorithme : calcul de la complexité
3. **Complexité d'un algorithme : ordres de grandeur**
4. Preuve d'algorithme

[![Vidéo 3 part 1 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1e.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1e.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 3 part 2 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1f.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1f.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 3 part 3 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1g.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1g.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
