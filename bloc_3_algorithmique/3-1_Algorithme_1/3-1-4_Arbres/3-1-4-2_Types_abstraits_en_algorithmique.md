## Types abstraits de données 2/10 : types abstraits en algorithmique

1. Introduction à la séquence 1/10
2. **Types abstraits en algorithmique 2/10**
3. Pourquoi, pour qui ? 3/10
4. La pile 4/10
5. La file 5/10
6. Arbres - définition, propriétés, comptage part1 6/10
7. Arbres - définition, propriétés, comptage part2 7/10
8. Arbres binaires 8/10
9. Parcours d'arbre 9/10
10. Feuilles étiquetées 10/10

[![Vidéo 2 B3-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S4-V2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S4-V2.mp4)

## Transcription de la vidéo 

(en cours de mise en place) 
