## L'expression d'un algorithme 3/4

1. Algorithme, vie courante, première approche 1/4
2. La conception d'un algorithme 2/4
3. **L'expression d'un algorithme 3/4**
4. Algorithmique positionnement et références 4/4

Dans les ouvrages algorithmiques, quelques principes

[![Vidéo 3 B3-M1-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S1-Video3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S1-Video3.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
