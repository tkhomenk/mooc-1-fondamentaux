## Le problème du tri : contraintes, propriétés attendues

- 3-1-3-1 Présentation
- 3-1-3-2 Recherche d'un élément, recherche dichotomique (itératif récursif, (diviser pour régner)
- 3-1-3-3 **Le problème du tri : contraintes, propriétés attendues**
- 3-1-3-4 Le tri par sélection (preuve, complexité)
- 3-1-3-5 Le tri par insertion (preuve, complexité)
- 3-1-3-6 Autres tris (bulle,...)

[![Vidéo 3 B3-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S3-video6.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S3-video6.mp4)

## Transcription de la vidéo 

(en cours de mise en place) 
