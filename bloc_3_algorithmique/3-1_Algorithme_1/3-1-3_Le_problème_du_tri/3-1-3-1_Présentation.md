## Le problème du tri :Présentation

1. **Présentation**
2. Recherche d'un élément, recherche dichotomique (itératif récursif, (diviser pour régner)
3. Le problème du tri : contraintes, propriétés attendues
4. Le tri par sélection (preuve, complexité)
5. Le tri par insertion (preuve, complexité)
6. Autres tris (bulle,...)

[![Vidéo 1 B3-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S3-video1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S3-video1.mp4)

## Transcription de la vidéo 

(en cours de mise en place) 
