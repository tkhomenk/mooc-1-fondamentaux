# Introduction à HTML 5 1/3: Base et structure d'une page web : le HTML

1. **Base et structure d'une page web : le HTML**
2. HTML : éléments de base du HTML
3. HTML : mise en forme et structures avancées

[![Vidéo 1 B4-M4-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M4-S1-video2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M4-S1-video2.mp4	)

## Transcription de la vidéo 

(en cours de mise en place)
