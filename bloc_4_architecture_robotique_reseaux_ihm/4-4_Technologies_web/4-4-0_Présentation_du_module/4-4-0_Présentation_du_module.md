# Présentation du module : Interface web (javascript, PHP..)

-  4-4-1 Introduction à HTML 5
   -  4-4-1-1 Base et structure d'une page web : le HTML
   -  4-4-1-2 HTML : éléments de base du HTML
   -  4-4-1-3 HTML : mise en forme et structures avancées
-  4-4-2 Déploiement d'un site web
-  4-4-3 Interactions et gestion dynamique dans une page web
-  4-4-4 Frameworks de développement et outils de gestion de contenu

# Objectifs du module
ce que vous allez apprendre

# Prérequis
Avoir suivi les modules Réseaux, BDD, python, OS

# Temps d'apprentissage

[![Vidéo 1 B4-M4-S0 Présentation du module ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M4-S1-video1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M4-S1-video1.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
