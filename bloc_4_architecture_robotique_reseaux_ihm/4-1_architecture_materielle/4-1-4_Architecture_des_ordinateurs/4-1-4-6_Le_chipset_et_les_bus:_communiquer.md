## Architecture des ordinateurs: Le chipset et les bus,communiquer

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. Introduction
2. La mémoire : plus et plus vite (4 videos)
- La hierarchie des mémoires, Les technologies SRAM et DRAM, les registres
- La mémoire centrale
- La mémoire cache & la mémoire virtuelle (2 videos)
- Les mémoires de stockage (1 vidéo)
3. La CPU : calculer plus vite** (2 vidéos)
4. **Le chipset et les bus : communiquer** (1 vidéos)

[![Vidéo 8 B4-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S4-V8.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S4-V8.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 9 B4-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S4-V9.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S4-V9.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
