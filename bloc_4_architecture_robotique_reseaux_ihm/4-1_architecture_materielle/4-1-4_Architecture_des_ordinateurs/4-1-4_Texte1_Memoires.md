# M4.1 Architecture

[//]: <> ()

## ch4 Architecture des ordinateurs

**diapo 4** Intro Objectif
Après avoir présenté le fonctionnement d'un système informatique minimaliste puis avoir détaillé certains aspects, notamment la pile et les interruptions à travers la présentation des systèmes embarqués, nous nous intéressons dans ce chapitre 4 aux ordinateurs.
Ces derniers sont des systèmes informatiques reprenant les éléments présentés dans les chapitres précédents.
Du PC portable du collégien aux puissants serveurs des data-center, les ordinateurs sont diffusés massivement et ont ainsi bénéficié de large financement pour leur développement. Ils ont ajouté à la base commune des systèmes informatiques des évolutions dans le but d'améliorer leurs performances.
Cette présentation se concentrera exclusivement sur cet objectif d'augmentation des performances, bien que d'autres problématiques d'encombrement ou de consommation électrique notamment conduisent à des progrès importants également en informatique. 
Ces 2 problématiques sont particulièrement fortes dans les ordinateurs portables et les smartphones, les récentes évolutions de ces derniers les rapprochant de plus en plus des premiers, comme l'a montré la présentation d'un SoC MediaTek dans le chapitre précédent.

Ce dernier chapitre sur l'architecture des PCs permettra à l'auditeur je l'espère de comprendre les spécifications techniques des machines pour faire des choix et les exploiter au mieux dans ses développements logiciels.

**diapos 5 et 6** Introduction
On reprend ici le système informatique minimal présenté dans les chapitres précédents, auquel nous avons ajouté quelques périphériques, en vert, pour se rapprocher de l'architecture des premiers ordinateurs. On distigue une mémoire centrale rapide mais volatile et une mémoire de stockage, plus lente mais non volatile (c'est-à-dire que les données y sont conservées lorsque l'alimentation est coupée). Dans le but d'améliorer les performances, ces PCs ont évolué, conduits essentiellement par les concepteurs de processeurs AMD, Intel et Nvidia et de mémoires Micron Technology et Samsung : 
(__CLIC__) Les CPUs des microprocesseurs se sont dotées d'instructions plus complexes, et de mécanismes comme les pipelines destinés à exécuter ces instructions plus rapidement.
(__CLIC__) Pour accélérer le fonctionnement des ordinateurs en parallélisant l'exécution des processus, Les concepteurs de processeurs ont inclus plusieurs CPU dans le même circuit intégré.
(__CLIC__) Sont aussi ajoutés sur certaines puces des coeurs spécialisés dans le rendu graphique, les GPU Graphic Processor Unit.
On voit ainsi qu'un circuit intégré (appelé aussi puce) Intel ou AMD contient bien plus qu'une simple CPU. Le terme SoC, System On Chip serait bien adapté. L'usage fait que l'on parle toujours de processeur pour désigner à la fois le circuit intégré mais aussi les coeurs qui sont à l'intérieur de celui-ci. Nous essaierons d'utiliser dans ce chapitre le terme micro-processeur pour le circuit intégré et le terme CPU pour les processeurs internes à celui-ci.
(__CLIC__) Sur les ordinateurs de bureau et les serveurs où place et consommation ne sont pas trop contraintes, il est possible d'ajouter une carte graphique avec un puissant GPU, sa mémoire et son refroidissement à l'extérieur du processeur, dialogant avec celui-ci par bus de données et bus d'adresse.
(__CLIC__) Ces bus ayant un débit limité au regard des vitesses des processeurs et les mémoires de grande capacité n'ayant pas gagné autant en rapidité que les processeurs, De la mémoire rapide est ajoutée sur la puce, comme sur les microcontrôleurs, au plus proche de la CPU. Cette mémoire, de petite taille, n'est qu'un tampon entre la CPU et la mémoire centrale, elle est nommée mémoire cache.
(__CLIC__) Pour tirer partie au mieux des architectures Von Neuman et Harvard, et ainsi charger en même temps instructions et données, on ajoute 2 mémoires cache, une pour les données, une pour les instructions, pour chaque CPU. C'est l'architecture Harvard modifiée.
(__CLIC__) Enfin, les différents périphériques ayant des vitesses différentes, pour ne pas ralentir les plus rapides, un composant nommé Chipset est ajouté pour faire l'intermédiaire entre le processeur et ces périphériques, avec des bus adaptés pour chacun. Ce chipset intègre aussi les périphériques classiques : réseau et son notamment.

Ce chapitre détaillera ces différentes améliorations.


**diapo 7** Plan
Avant d'entrer dans les évolutions des processeurs et de présenter le chipset, les bus de communication et la carte mère,
(__CLIC__) Nous commencerons par détailler les différents types de mémoire d'un ordinateur.

**diapo 8** Intro Plan - Mémoires
Dans cette première partie, nous parcourerons les différents types de mémoire des ordinateurs.
Après un court rappel de vocabulaire, nous présenterons les technologies SRAM et DRAM puis les registres et la mémoire centrale.
La vidéo suivante sera consacrée à la mémoire cache et à la mémoire virtuelle.
Enfin, la dernière vidéo présentera les mémoires de stockage.

**diapo 9** vocabulaire

Commençons par présenter le vocabulaire utile, et les concepts associés.
Une mémoire est un composant sur lequel on stocke, on conserve et on relit des données. Les informations y sont codées en binaire, sous forme de 0 et de 1. On accède à ces informations par octet.

(__CLIC__) Une **mémoire volatile** ne conserve pas les informations lors d'une absence d'alimentation, au contraire d'une mémoire non volatile.

(__CLIC__) **RAM** signifie Random Access Memory. Une mémoire RAM est une mémoire où l’accès direct à toutes les cases est possible, le temps d’accès est le même quel que soit le mot concerné. Le terme Random signifie qu'il est possible d'accéder à n'importe quelle case choisie, pas que l'accès se fait au hasard. Les mémoires à semi-conducteurs, volatile ou non, sont habituellement des mémoires RAM. L'usage utilise ce terme pour désigner la mémoire centrale d'un ordinateur, qui est bien de la RAM mais pas plus que la mémoire cache du processeur.

(__CLIC__) **ROM** signifie Read Only Memory. Auparavant cela désignait une mémoire écrite une seule fois et non volatile, dans laquelle était stocké le programme d'un système informatique. Depuis l'apparition des disques dur, la ROM stockait uniquement le petit programme de démarrage BIOS des PCs. Aujourd'hui, avec l'apparition des mémoires Flash, les mémoires ROM sont réinscriptibles électroniquement, ce qui permet notamment la mise à jour du BIOS d'un ordinateur, mais l'usage du terme ROM perdure. Nous l'utiliserons peu.

Notons que ROM et RAM ne sont pas des termes antagonistes. Certaines mémoires Flash sont aussi des mémoires RAM car il est possible d'accéder à n'importe quel octet. Une mémoire Flash est aussi une ROM au sens élargi du terme, c'est-à-dire une mémoire non volatile.

(__CLIC__) **La Latence**, ou temps d'accès, est la durée s’écoulant entre la demande de lecture ou d’écriture d’une donnée et la réalisation de cette action.

(__CLIC__) **Le Débit ou la Bande passante** sont le nombre d'octets maximum transférés par seconde. C'est le produit de la largeur du bus de données et de la fréquence de la mémoire exprimé souvent en MegaTransfer/s

La latence n'est pas l'inverse du débit. En effet, sur un disque dur, cas le plus caractéristique, il est très long d'accéder à une première donnée car la tête de lecture doit se positionner mécaniquement à l'endroit où est stockée cette donnée. La latence est donc élevée, de l'ordre de 20 ms. Une fois la tête positionnée, les données contigue arriveront rapidement.

(__CLIC__) **La Mémoire de travail** est la mémoire accessible directement à la CPU, ce qui conmprend les registres, la mémoire cache et la mémoire centrale.

(__CLIC__) **La Mémoire de stockage** est une mémoire plus lente, non volatile et de plus grande capacité, accessible via un périphérique. C'est sur un ordinateur un SSD ou disque dur.

(__CLIC__) Enfin, la **Capacité** désigne la quantité d’information que la mémoire peut stocker (en octets).


Parmi les caractéristiques d'une mémoire, outre son caractère volatile ou non volatile, sa capacité, sa latence et son débit, on peut ajouter le coût par octet stocké et le nombre de cycle de lecture écriture possibles.

**diapo 10** Hiérarchie des mémoires

Les programmes étant de plus en plus complexes et les fichiers multimédias de taille toujours plus importante, pour fonctionner rapidement, les ordinateurs ont besoin de mémoire de grande taille et rapide, ce qui ne va pas bien ensemble.
L'antagonisme entre taille et rapidité est résolu en partie par l'usage complémentaire de mémoires petites et rapides, proche de la CPU et de mémoires plus lentes et de grande capacité, plus éloignées. C'est ce qu'on appelle la hiérachie des mémoires.

La vitesse du signal électrique est environ 2.10⁸ m/s sur le cuivre. Il faut donc 2 ns au signal électrique pour faire les 20 cm séparant la CPU de la mémoire centrale.
Les connexions sur les puces, pour des raisons technologiques sont un peu moins rapides, mais les distances plus faibles. Les temps de transit d'un bout à l'autre de la puce sont aussi de l'ordre d'une ns.

Ainsi, plus la case mémoire est proche de la CPU, plus le temps d'accès est réduit.

Cependant la mémoire occupe de l'espace, on ne peut donc mettre qu'une quantité limitée dans le coeur du processeur, ce sont les registres CPU. Ces registres sont très peu nombreux.

La mémoire externe, nommée mémoire centrale ou mémoire primaire, utilise une technologie plus lente, mais moins chère et plus dense que la mémoire embarquée, la DRAM, que nous présenterons à la diapositive suivante. 

(__CLIC__) Pour accélérer le fonctionnement de la CPU, on rapproche données et programme via de la mémoire embarquée sur la puce, proche de la CPU, c'est la mémoire cache.

Enfin, pour stocker les données de manière pérenne, les ordinateurs utilisent des mémoires non volatiles, appelées mémoire de masse, mémoire secondaire ou mémoire de stockage, peu rapides avec un nombre d'écritures limité mais avec un prix à l'octet faible.
Il y a 15 ans, l'ensemble était regroupé sous le terme disque dur.
Avec les progrès des mémoires flash sont apparus les disques SSD de taille raisonnable, les disques durs magnétiques sont réservés au stockage des grandes capacités, donc plutôt aux stockage des données, notamment les données multimédia, gourmandes en espace.
Ces mémoires de stockage étant relativement lentes, elles utilisent un bus de communication SATA contrôlé par le chipset.

(__CLIC__) (__CLIC__) On peut représenter cette hiérarchie par une pyramide commentée regroupant les performances des 5 types de mémoire d'un ordinateur. Les prix au Go, calculés en 2021, sont en baisse constante. Cette information sera périmée rapidement mais les écarts relatifs resteront significatifs encore longtemps.
(__CLIC__) Les registres sont accessibles pendant le cycle instruction mais en quantité limitée : 1 ko
(__CLIC__) Un microprocesseur moderne contient plus de 10 Mo de mémoire cache, accessible avec une latence comprise entre 1 et 10 ns, suivant le niveau. Le débit et les prix sont extraits de la documentation d'une mémoire MoSys externe de type SRAM comme la mémoire cache.
(__CLIC__) La mémoire centrale est couramment de 16 Go avec un coût en baisse continu de 15 euros par Go environ. Les temps de latence mesurés varient entre 40 et 70 ns.
(__CLIC__) Un disque SSD a un temps d'accès de 60 µs en lecture et un débit allant de 4 à 24 Gb/s suivant le bus de communication, pour 20 centimes du Go.
(__CLIC__) Enfin, un disque dur a un temps d'accès très long à l'échelle du cycle instruction, 5 ms, un débit assez faible de 1,5 Gb/s mais un prix de 4 centimes d'euros du Go.

[//]: <> (Les chiffres ,Ada dessous et mesures MLC, ne sont pas aussi optimistes. Les benchmarks donnent 60 ns pour la latence mémoire RAM (mes chiffres mlc semblent corrects -> Outil Intel Memory Checker)

**diapo 11** techno 
Le chapitre 2 a montré que on pouvait faire des mémoires à accès direct avec des bascules et des démultiplexeurs. C'est la mémoire RAM (Random Access Memory). Le terme random signifie que le système peut demander à tout instant une lecture ou une écriture de n'importe quelle case de cette mémoire.
2 technologies de mémoires volatiles et rapides coexistent, avec leurs avantages et leurs inconvénients :
la mémoire statique SRAM et la mémoire dynamique DRAM.

La SRAM utilise des transistors, comme le processeur. Sa vitesse évolue comme celle du processeur. 
(__CLIC__) On peut simplifier les deux portes NON réalisées chacune avec 2 transistors, il faut 6 transistors par bit mémoire.

La DRAM utilise quant à elle le principe de la charge d'un condensateur via 1 transistor. Le condensateur est de quelques dizaines de femtoFarad, c'est-à-dire de l'ordre de 10^-14 F. Celui-ci est réalisé avec des diélectriques incorporés dans le silicium. 
L'inconvénient majeur de ces mémoires est lié aux fuites du condensateur. La mémoire doit rafraichir les charges des condensateurs régulièrement, ce qui consomme du temps et de l'énergie. Micron Technology annonce 4 µs de période de rafraichissement pour sa récente DDR5.
Un compromis doit donc être trouvé pour la taille du condensateur. Si il est trop petit, il perd rapidement sa charge => il faut rafraichir souvent. Si il est trop gros, on met trop de temps à charger décharger, ce qui ralentit les écritures.

Un seul transistor, plus un très petit condensateur sont nécessaires pour un bit mémoire, ce qui permet des densités plus élevées et un coût plus faible.

(__CLIC__) On voit ainsi apparaître las avantages et inconvénients des 2 technologies. La SRAM est plus rapide et très fiable, alors que la DRAM est plus dense et moins chère.

(__CLIC__) La SRAM est donc utilisé pour les registres et mémoire cache inclus sur les circuits intégrés des microprocesseurs, mémoires petites et à la latence très faible. 
(__CLIC__) La photo coloré représente la matrice silicium d'un processeur mobile quadricoeur.
(__CLIC__) On y voit des zones de surface non négligeables, occupées par la mémoire cache de niveau 3. 

(__CLIC__) La DRAM est elle utilisée pour la mémoire centrale, à l'extérieur du processeur. On donne les caractéristiques des récentes DDR5.
(__CLIC__) La photo montre une barette rassemblant plusieurs circuits de mémoire DRAM. L'échauffement des récentes mémoires DDR4 et DDR5 amène à équiper ces barettes d'un dissipateur thermique métallique.

Détaillons ces différentes mémoires de travail.

**diapo 12** Les registres
Les registres ont un temps d'accès de quelques dizaines de ps, beaucoup moins qu'un cycle instruction.
La diapositive présente les 16 registres 32 bits de travail d'un cortex M0+, dont les registres spéciaux Stack Pointer et Program Counter.
S'ajoutent le registre de statut et 7 registres spécifiques.

(__CLIC__) Sur un processeur Intel amd64, on retrouve les 16 registres de travail, le registre instruction et le registre de statut. Les registres de travail sont en nombre limité car ils doivent être sauvegardés à chaque changement de contexte, lors de l'apparition d'une interruption ou lors d'un changement de tâche.

(__CLIC__) S'y ajoutent des registres spécifiques à l'unité de calcul en virgule flottante et (__CLIC__) des registres de taille supérieure, jusqu'à 256 bits pour l'unité de calcul sur plusieurs nombres simultanément.

**diapo 13** La mémoire centrale 
La mémoire centrale utilise des circuits de DDR SDRAM.
Double Data Rate indique que le circuit transmet les données sur front montant et descendant d’horloge.
Synchronous signifie que le circuit attend un front d’horloge pour prendre en compte l’état des signaux d’entrées.
enfin, DDR5 est le nouveau standard avec un débit de 4800 MegaTransfert/s, soit 38.4 Go/s pour une barette 64 bits. La latence quant à elle progresse peu par rapport aux DDR3 et DDR4, avec 15 ns annoncé au niveau du circuit et 40 ns environ mesurés en pratique.

(__CLIC__) La mémoire se présente sous forme de barette de plusieurs circuits : DIMM Dual Inline Memory Modules, avec un dissipateur thermique métallique pour les récentes barettes DDR4 et DDR5.
(__CLIC__) on la trouve aussi au format SODIMM :  Small Outline DIMM, pour les ordinateurs portables. Sur les smartphones, les circuits LPDDR, (pour Low Power DDR) sont soudés sur le circuit imprimé.
(__CLIC__) les mémoires RDIMMs sont les mémoires avec un buffer entre la mémoire et le bus, ce qui permet de mettre plus de circuits mémoire. C'est utilisé sur les serveurs essentiellement.
L'extension ECC pour Error Correction Code est l'ajout d'un mécanisme de vérification d’erreur, un peu comme un bit de parité. Cela demande un circuit de mémoire supplémentaire et quelques lignes supplémentaires sur le bus mémoire. 80 à la place de 64 pour un bus 64 bits.
Le bus reliant les mémoires au processeur a une grande vitesse et est très utilisé. Les microprocesseurs sont pour cela dôtés d'un contrôleur mémoire (IMC pour Integrated Memory Controller) gérant les échanges sur un voir 2 canaux de bus réservé à la mémoire.

**diapo 14** La mémoire centrale - temps de propagation
Notons qu'à 4 GT/s, soit 4.10^9 échanges par seconde, le temps de propagation sur les lignes ne peut être négligé, Nous ne sommes plus dans l'approximation des régimes quasi-statiques, usuelle pour l'électronique classique. L'image montre le soin apporté au design de la carte mère pour que toutes les lignes entre le microprocesseur et la mémoire fassent la même longueur.
------------------------------------------------------------------------------------------
**diapo 15** Rappel plan
Dans cette vidéo, nous présenterons la mémoire cache, située entre les deux mémoires de travail présentées précédemment : les registres et la mémoire centrale. Nous expliquerons ensuite la mémoire virtuelle.

**diapo 16** mémoire cache

Comme nous l'avons dit dans la vidéo précédente, la mémoire centrale, très rapide et très sollicitée a son propre bus, le bus mémoire avec signaux de contrôle, données et adresses, pour échanger avec le processeur. Ce bus est géré par un contrôleur mémoire nommé IMC en anglais : Integrated Memory Controller

Le processeur tourne à 4 GHz. Pour éxécuter une instruction par cycle, il faudrait chaque 0,25 ns charger une instruction 64 bits et 1 voir 2 données 64 bits, soit un débit nécessaire de 768 Gbits/s, et même plus si ce sont des instructions SIMD, simultanées sur plusieurs données.
(__CLIC__) En plus du débit inférieur, le temps d'accès à la mémoire centrale, de l'ordre de 40 ns est largement insuffisant.
En effet, pour des calculs complexes, les processeurs et GPU sont devenus tellement performants que le goulot d'étranglement est l'accès mémoire. Un processeur passe en moyenne la moitié de son temps à faire des copies de données d'une mémoire de travail à une autre (registre, mémoire cache, mémoire centrale).

Au début de l'informatique, les durées d'un cycle instruction des processeurs étaient compatibles avec les temps d'accès des mémoires. Ensuite, cette durée du cycle instruction a diminué beaucoup plus vite que les temps d'accès des mémoires. Pour ne pas ralentir l'amélioration des performances des ordinateurs, à la fin des années 80, est apparue la mémoire cache.

(__CLIC__) La mémoire cache de niveau 3 quand elle est présente est celle qui est en lien avec la mémoire centrale. Elle est commune à l'ensemble des coeurs du microprocesseur. Elle contient une copie des données et instructions de la mémoire centrale jugées intéressantes pour l'exécution des programmes en cours.
D'une taille de plusieurs Mo, elle est relativement éloignée sur la puce des CPU, son temps d'accès est de l'ordre de 10 ns sur un processeur moderne.
(__CLIC__) Les valeurs données sont celles de mon PC portable de 2020 avec processeur i7 10ème génération.

(__CLIC__) Pour améliorer le temps d'accès, un autre niveau de mémoire cache est présent, plus proche et propre à 1 ou 2 CPU, la mémoire cache de niveau 2. 
(__CLIC__) Pour accélérer les échanges entre les 2 mémoires caches, des canaux spéciaux, de largeur de 16 ou 32 octets, soit 256 bits, sont créés.

(__CLIC__) Chaque CPU ou groupe de 2 CPU possède sa mémoire cache de niveau 2, 
(__CLIC__) avec son canal d'échange avec la cache de niveau 3.

(__CLIC__) Le temps d'accès de cette relativement large mémoire située à la périphérie de la CPU étant encore trop important, 

(__CLIC__) Une mémoire cache de niveau 1, propre à chaque CPU est ajoutée.
(__CLIC__) Pour chaque processeur.
(__CLIC__) Intégrées à l'empreinte du CPU sur le circuit intégré et de taille limitée, le temps d'accès est de l'ordre d'une ns.
Des canaux spéciaux de 16 à 32 octets achemine les données depuis et vers la cache de niveau 2.
La mémoire cache de niveau 1 est habituellement séparée en cache instruction (très utile pour les boucles) et cache données (très adaptée aux données en table notamment). Cela permet de charger instructions et données en même temps.
Le temps d'accès à cette petite mémoire cache est de 1 ns environ.

D'autres mécanismes nous le verrons permettent de gagner en temps cycle instruction. Ce temps d'accès de 1 ns est le temps d'accès actuel d'un processeur aux instructions et aux données, hors registres internes.

Lorsque le processeur lit la donnée dans le cache de niveau 1 :
- si la donnée est présente, il obtient celle-ci très rapidement en 3 cycles d'horloge à peu près.
– si la donnée n’est pas présente dans la mémoire cache, un défaut de cache a lieu (cache miss en anglais) ;
– ce défaut de cache déclenche alors la copie de la donnée depuis la mémoire cache de niveau 2, ou 3 ou depuis la mémoire centrale vers la mémoire cache de niveau 1, ainsi que les données autour ;
– la donnée peut alors être lue par le processeur, et les données autour seront disponibles en cas de besoin. En effet, si une donnée est demandée, il est fort possible que les données autour soient sollicitées prochainement. Les données sont donc copiées par page de la mémoire centrale à la mémoire cache de niveau 3.
Ensuite, les pages sont plus petites pour la cache de niveau 2 et on parle de ligne pour la cache de niveau 1.
Il se passe la même chose pour les instructions si ce n'est que ce sont uniquement les instructions suivantes qui sont chargées en cache, celles-ci étant les plus à même d'être exécutées ensuite.

On a ainsi 5 zones de mémoires de travail différentes  : Registres, cache de niveaux 1, 2 3, et mémoire centrale.
Le processeur gère lui-même les mémoires cache, c'est transparent pour le développeur. Toutefois, nous verrons dans la conférence 3 qui suit ce cours comment le développeur peut optimiser son code pour que, en passant par le compilateur, il exploite au mieux la mémoire cache.

**diapo 17** mémoire cache -  – principe de localité
2 principes régissent comment le processeur choisit ce qu'il copie en mémoire cache :
* Le principe de localité temporelle indique que si une information (instruction ou donnée) est manipulée par le processeur, il est très probable qu’elle sera également utilisée peu de temps après.
* Le principe de localité spatiale indique lui que si une information est manipulée par le processeur, il est très probable qu’une information située dans un emplacement mémoire proche sera également utilisée peu de temps après.

Notons aussi que la mémoire cache utilise une mémoire associative : à chaque ligne de cache (de plusieurs octets) correspond une étiquette avec l'adresse qu'elle remplace. Une électronique de comparateur fait que la validation de la présence en cache d'une donnée est extrêmement rapide.

**diapo 18** mémoire cache

Observons cela en mouvement.

Au démarrage du PC, tout est stocké sur le disque dur. Celui-ci étant d'un accès lent, en lecture notamment, dès qu'un programme est lancé, il est chargé en mémoire centrale. (__CLIC__), tout comme les données qu'il utilise (__CLIC__). Le système étant multitâche, plusieurs programme sont chargés en mémoire centrale simultanément (__CLIC__). Avec l'augmentation des capacités mémoire, des systèmes rapides ont le noyau complet et les quelques programmes qu'ils utilisent chargés en mémoire centrale. Les blocs chargés en mémoire centrale sont de grande taille.

La CPU demande l'instruction de début du programme. Pour espérer de bonnes performances, (__CLIC__) le bloc d'instructions i1 et les blocs suivants sont stockées dans la mémoire cache de niveau 3, puis une partie dans la cache de niveau 2 (__CLIC__)et enfin les blocs i1 et i2 arrivent dans la cache de niveau 1 (__CLIC__).
Le programme demande alors les données présentes dans le bloc d2. Les données autour seront probablement appelées également, c'est pourquoi les bloces d1 à d5 sont copiés en cache de niveau 3 (__CLIC__), les blocs d1 à d3 en cache de niveau 2 (__CLIC__) et enfin les blocs d2 et d3 arrivent en cache de niveau 1.

La CPU 2 exécute en parallèle le programme 2 (__CLIC__), qui est copié en cache. Celui-ci demande aussi un accès aux données du bloc d2. Le mécanisme de gestion de la mémoire cache bloque l'accès aux données car elles sont déjà dans la mémoire cache de la CPU 1. Il y a un risque d'incohérence si les données sont modifiées en même temps par 2 CPU. L'utilisation de la mémoire cache est donc plus complexe que la déjà difficile prévision des données et instructions susceptibles de servir prochainement.

Si la CPU 1, exécute une instruction de saut du bloc i1 du programme 1 vers une instruction du bloc i2, l'instruction est dans la mémoire cache de niveau 1, c'est très rapide. Il y a succès de cache.
Par contre, si le saut est vers une instruction du bloc 4, elle n'est pas dans le cache de niveau 1, il faut aller la chercher dans la mémoire cache de niveau 3 (__CLIC__), ce qui ralentit beaucoup le programme, 30 cycles. Pire, si elle n'est pas dans le cache de niveau 3, par exemple dans le bloc 6, il faut aller jusqu'en mémoire centrale, ce qui provoque un retard de 150 cycles (__CLIC__). On parle de défaut de cache.
Ces délais difficiles à prévoir lorsque le programme est grand et utilise de nombreux sauts, montre l'intérêt de la cache de niveau 3, même plus lente et montre aussi qu'il est impossible dans ces conditions de prévoir précisément le temps d'exécution d'un programme.

**diapo 19** mémoire cache de mon PC

Le programme intel mlc (Memory Latency Check) permet de mesurer les temps de latence sur des copies d'une mémoire de travail à l'autre.
(__CLIC__) Mon PC Dell possède un Intel i7 ayant 128 ko de mémoire cache de niveau 1, 1 Mo de cache de niveau 2 et 8 Mo de cache de niveau 3. La mémoire est de la DDR4 à 3200 MTransfert/s
(__CLIC__) on mesure 113 ns pour une copie de la mémoire centrale à la mémoire centrale et 26 ns de la cache de niveau 2 à la cache de niveau 2.
Si on suppose le temps d'écriture égal à celui de lecture, on obtient 13 ns de temps d'accès à la cache et 56 ns à la mémoire centrale.
(__CLIC__) Le débit est de 11 Go/s, soit 88 Gbits/s. Sur un autre machine, de type station de travail, avec 2 canaux de mémoire, occupés chacun par une barette de mémoiore DDR4, on mesure un débit 4 fois plus élevé.

**diapo 20** Mémoire virtuelle - Unité de Gestion de la Mémoire MMU
Nous avons jusqu'à présent défini une adresse comme le numéro d'un octet disponible dans la mémoire centrale du système informatique. Nous appellerons
désormais ces adresses des « adresses physiques » dans la mesure où elles font
directement référence à la réalité physique de la machine. 
Les adresses des microprocesseurs modernes utilisent plus de 40 bits. Sur mon intel i7 de 10ème génération, l'espace adressable est sur 48 bits, soit plus de 280 milliards d'adresses, ce qui correspond à 512 fois l'espace maximal en mémoire physique sur 39 bits soit 512 Go.

C'est pourquoi, il est possible de profiter de ce large espace pour allouer à chaque processus un espace continu de la taille de la mémoire centrale, comme nous le verrons dans le module systèmes d'exploitation.
De telles adresses n’ayant pas de correspondance physique s’appellent des
adresses virtuelles.
(__CLIC__) Le système de gestion de la mémoire (MMU pour Memory Management Unit), élément du processeur, fait alors un lien très rapide entre les adresses virtuelles utilisées et l'espace alloué dynamiquement en mémoire physique, qui peut être non continu.
Ce lien s’appelle la traduction d’adresses. La correspondance entre adresse physique et adresses virtuelles utilise un buffer nommé TLB Translation Look-aside Buffer. Chez Intel, la correspondance TLB a lieu entre la CPU et la mémoire cache. Les adresses utilisées comme étiquette en mémoire cache sont les adresses physiques.

**diapo 21** Mémoire virtuelle - Unité de Gestion de la Mémoire MMU
Prenons un peu d'avance sur le cours de systèmes d'exploitation pour présenter le principe de la mémoire virtuelle.
(__CLIC__) Lors de la création d'un processus, un espace de mémoire virtuelle est alloué au processus.(__CLIC__) 

(__CLIC__) (__CLIC__) Les instructions, les bibliothèques et les données sont copiées de la mémoire de stockage dans cet espace mémoire virtuel. La pile et le tas prennent place également dans cet espace.

(__CLIC__) Pour cela, le gestionnaire de mémoire du processeurs MMU fait la correspondance entre cet espace mémoire virtuel et des zones libres de la mémoire centrale. (__CLIC__) (__CLIC__) 
La translation d'adresses par la MMU vers la mémoire physique se fait habituellement par page de 4 ko.

Un processus peut demander un supplément de mémoire pour le tas.(__CLIC__) (__CLIC__) Il obtient alors un supplément de mémoire contigu dans son espace virtuel alors que physiquement cette extension du tas est situé à un autre endroit de la mémoire. 

Lors de la création d'un second processus (__CLIC__) , un second espace de mémoire virtuel est créé (__CLIC__), espace qui peut partager la zone de mémoire de certaines bibliothèques avec le premier. 

Si la mémoire physique est pleine, le MMU peut libérer de la mémoire physique en stockant provisoirement sur le disque, dans une zone nomméee swap, les pages correspondant à l'espace adressable d'un processus prêt ou bloqué. (__CLIC__) 

Si la mémoire centrale est pleine, tout comme la partition de swap, il n'est plus possible de créer de nouveaux processus. (__CLIC__) Vu les tailles des mémoires aujourd'hui, ce message est peu connu des plus jeunes.

Par ailleurs, la pagination de l'espace mémoire facilite l'existence de la mémoire virtuelle en traduisant les zones de mémoire par page de 4 ko.

Enfin, notons que la zone de swap est aussi utilisée pour sauvegarder le contenu de la mémoire centrale lors des phases de sommeil profond du PC (hibernation, mise en veille prolongée), pour permettre un redémarrage très rapide et dans le même état qu'au moment de la mise en sommeil.
-----------------------------------------------------------------------------------------
**diapo 22** Rappel Plan
Après avoir présenté les mémoires de travail dans les 2 vidéos précédentes, terminons cette partie en abordant les mémoires de stockage.

**diapo 23** La mémoire de stockage - technologies
2 technologies principales existent actuellement, les disques durs magnétiques et les disques SSD.

(__CLIC__) Les disques SSD sont une mémoire flash à transistors avec grille isolée. L'information y est stockée de manière durable, c'est une mémoire non volatile.
La lecture se fait comme pour une mémoire classique, mais l'écriture demande sur la grille de contrôle une tension importante, ce qui fait que le nombre d'écritures est limité. Les disques annoncent aujourd'hui 500 000 cycles d'écriture possibles.

(__CLIC__) En plus des transistors, le disque contient un contrôleur pour veiller à ce que les écritures utilisent tout le disque et non une petite zone qui faiblirait rapidement. Si une zone meure, il dispose d'un espace surnuméraire de mémoire à la fabrication du disque, qu'il peut allouer pour compenser les zones mortes. 

(__CLIC__) Pour gagner en vitesse, en écriture surtout, on ajoute une mémoire cache en tête de disque. Le disque SSD, qui n'a rien d'un disque, se comporte de la même manière qu'un disque dur, avec un formatage et des écritures/lectures par secteurs.

(__CLIC__) 
Un disque magnétique dispose quant à lui de plusieurs plateaux en aluminium, verre ou céramique, recouverts d’une ﬁne couche d’un substrat magnétisable, sur laquelle les données sont enregistrées. 
La tête de lecture/écriture survole la surface du disque. 
En écriture, le courant envoyé dans la tête modiﬁe le champ magnétique local pour écrire soit 0, soit 1. 
En lecture, Ce champ magnétique local induit une tension aux bornes de la tête.
Un bloc d'informationn est localisé par :
– un numéro de tête, qui détermine le choix d’une surface, chaque plateau ayant une surface supérieure et une surface inférieure,
– un numéro de piste, qui détermine le cylindre,
– un numéro de secteur sur cette piste, qui détermine à partir de quel endroit commencer à lire ou écrire les données pendant une rotation.

D'énormes progrès ont été fait sur la densité d'informations. Les premiers PC dans les années 90 avaient des disques durs de 20 Mo et, pour le même prix, les nouveaux PC, 30 ans plus tard peuvent facilement s'équiper de 20 To, 1 million de fois plus !

Le disque dur n'est pas à accès aléatoire. Donc le temps de latence est très long à l'échelle du cycle instruction, il faut attendre le disque et déplacer la tête. Un calcul rapide indique qu'un disque tournant à 6000 tr/min fait 100 tr/sec donc un tour en 10 ms. En moyenne, il faut donc 5 ms pour accéder à un secteur.

Une fois la première donnée lue, pour des données contigues, on peut atteindre un débit de 200 Mo/s.
On voit ainsi que pour être efficace, il faut que les données d'un même fichier soient contigues sur le disque. La dispersion des éléments d'un même fichier sur différents secteurs s'appelle la fragmentation, elle est très nuisible au débit. Avec un temps de latence de 5 ms, on passerait pour des données contingues à des données complètement fragmentées de 200 Mo/s à 200 octets/s.
Pour limiter la fragmentation, les données sont inscrites par blocs sur le disque. Ces blocs ne peuvent être scindés. Il faut donc trouver un compromis entre des blocs trop gros qui limitent la fragmentation mais occupent mal l'espace et des blocs plus petits qui occupent mieux l'espace mais permettent de la fragmentation.

(__CLIC__) Pour augmenter le débit et diminuer le temps de latence, comme sur les disques SSD, on ajoute de la mémoire cache.

(__CLIC__) Resortent ainsi les avantages des disques SSD, plus rapides et robustes grâce à l'absence de pièces en mouvement, antagonistes à ceux des disques durs magnétiques, ayant une très forte capacité à faible coût. Il est envisageable qu'ils se fassent rattraper par les disques SSD.


**diapo 24** La mémoire de stockage – 2 technologies 2
Un passage chez un marchand en ligne permet de voir les disques en rayon et de comparer les prix.
Les bus NVMe et SATA de connexion des disques au chipset seront présentés dans la partie 3 de ce cours.
Les disques durs magnétiques sont vendus uniquement sur bus SATA, ayant un débit maximal de 6 Gb/s, au-delà des performances des disques. Le coût du Go est très faible.
Les SSD ont fait des progrès en terme de prix et de capacités disponibles. On les trouve au format disque dur 2 pouces et demi, sur bus SATA, comme les disques magnétiques.
Profitant de la densité des mémoires flash, pour bénéficie de leur débit important, les circuits intégrés de mémoires flash sont positionnés sur des barettes communiquant via le très rapide bus PCIe, comme les cartes graphiques. Le débit peut atteindre 24 Gbits/s. Présentés sous forme d'une barette mémoire, ces mémoires de stockage peuvent difficilement justifier leur commune appelation disque SSD. 

**diapo 25** La mémoire de stockage – RAID
Pour améliorer le stockage, 3 architectures RAID permettent des fonctionnalités différentes.
L'architecture RAID 0 répartit les données sur deux disques de manière entrelacée. Il est alors possible de lire ou écrire un fichier qui serait réparti sur les deux disques. Les blocs sont répartis de manière alternée sur les disques 0 et 1. En faisant des accès simultanés aux deux disques, on bénéficie d'un débit en lecture comme en écriture multiplié par deux.
L'architecture RAID 1 ou mirroring, consiste à copier les informations sur les deux disques. On parle d'architecture redondante. La panne d'un disque alertera sur l'usure des disques mais sans désagrément, les données étant stockées aussi sur le second disque.

L'architecture RAID 5 combine la répartition des blocs du RAID 0 et une redondance de type parité.
Il faut N +1 disques pour disposer de la capacité utile de N disques. Par exemple, quatre disques pour trois utiles dans la configuration présentée.
En cas de défaillance d'un disque, il est possible de reconstruire toutes les données à partir des N disques restants.

Les technologies RAID ou similaires sont utilisées par les systèmes de stockage en réseau : NAS pour Network Attached Storage. Ces systèmes peuvent être personnels ou dans le cloud.

**diapo 23** Intro au chapitre sur les améliorations de la CPU
Nous avons vu dans le chapitre précédent comment l'ordinateur exploitait les différentes technologies de mémoire SRAM, DRAM, flash et magnétiques pour la hiérarchie mémoire : registre - cache - mémoire centrale - mémoire de stockage.
(__CLIC__) Encore dans le but d'améliorer les performances, la CPU des processeurs est elle aussi l'objet de nombreuses innovations. Nous verrons successivement comment les instructions complexes, les pipelines, les processeurs superscalaire, l'hyperthreading et les processeurs multicoeurs ont continué à doper les performances des ordinateurs, magré le mur de chaleur.

**diapo 24** Le temps d'exécution d'un programme est égal au nombre d'instructions divisé par le nombre d'instructions par cycle horloge multiplié par la fréquence d'horloge.
(__CLIC__) La vitesse d'horloge ne peut être raisonnablement augmentée au-delà de 5 GHz sans coûteux moyens de refroidissement. De plus, les temps de propagation à l'intérieur même du microprocesseur rendent délicat le fait de fonctionner à des fréquences plus élevées.
(__CLIC__) Il faut donc diminuer le nombre d'instructions, ce qui peut être fait en créant des instructions complexes capables de traiter les nombres en lot.
(__CLIC__) On peut aussi augmenter le nombre d'instructions par cycle horloge, ce qui est l'objectif annoncé de plusieurs technologies : pipeline, processeurs superscalaires, multi-coeurs et hypertreading.

**diapo 23** Instructions complexes
Une machine de Turing universelle peut, à partir de quelques instructions, exécuter toute opération calculable, au prix d'un temps plus ou moins important. C'est ainsi qu'une division de deux réels par un processeur n'ayant que des blocs de calcul sur des entiers est possible mais très longues, cette division devant se traduire par plus de 1000 instructions sur des entiers.
(__CLIC__) Rapidement a donc été ajoutée à l'ALU une unité de calcul auxiliaire pour le calcul en virgule flottante (FPU Flottig Point Unit).
Les calculs pour le rendu graphique utilisant massivement les matrices, sont apparus des unités de calcul sur des vecteurs SSE (Streaming SIMD Extensions) et AVX (Advanced Vector Extensions), pour calculer sur des vecteurs également mais plus gros (__CLIC__).
Les instructions SSE, communes à Intel et AMD, permettent des calculs sur des nombres réels ou entiers 128 bits par 128 bits.
Les instructions AVX, Advanced Vector Extension utilisent des (registres de 256 bits) voir même de 512 bits pour les dernières versions. La multiplication de 16 nombres réels simple précision par 16 nombres réels est faite en un cycle instruction.

On peut revenir ici sur la définition de RISC et CISC.
* Un processeur RISC eécute tout instruction, relativement simple, en 1 seul cycle.
Le format des instructions est simplifié et le jeu d'instructions réduit.
Les processeurs RISC font une utilisation intensive des registres accessibles en 1 cycle.

Surun processeur CISC, certaines instructions sont complexes, décomposées en micro-instructions par le décodeur de l'unité de contrôle.

Aujourd'hui, ces instructions complexes sont toujours décomposés en micro-instructions, mais exécutées suffisamment rapidement par la CPU que le tout se fait en un cycle instruction.
Il devient donc difficile de ranger les microprocesseurs modernes dans la catégorie CISC ou RISC.

Les instructions complexes sont prises en compte par le compilateur. Si on fait une div de réel; le compilateur choisira une instruction du bloc FPU. Par contre, pour exploiter au mieux le parallélisme, la manière de programmer et d'organiser les données compte aussi. 
La mise en oeuvre des instructions SIMD est l'objet de la 3ème conférence qui suit ce cours.

**diapo xx** Pipeline

* Aller chercher en mémoire l'instruction : Fetch instruction
* décoder le code opération : Decode
* Aller chercer le ou les opérandes : Fetch Data
* Effectuer l'opération : Execute
* Ecrire le résultat : Write result


Issu de Intel :
Each processor contains from one to hundreds of cores. Each core contains hardware to:

    Fetch instructions.
    Decode those instructions.
    Schedule the instructions for execution.
    Execute the instructions.
    Fetch the data the instructions need.
    Store the data the instructions produce.



Problèmes possibles (provoquent bubble ou pipeline stall): 
conflit de ressources (d'où l'intérêt de séparer cah I et cache D) : 2 instructions veulent accéder à cache D (une pour écrire resultat, une pour lire opérande par exemple)
dépendance de donnée : si une instruction utilise comme opérande le résultat d'une instruction précédente, il faut attendre que la première soit finie pour charger les opérandes de la seconde.
dépendance d'instruction  si une instruction est un saut conditionnel, ce n'est qu'une fois l'instruction exécutée que l'on connaît l'instruction suivante.
Souvent, on remplit tout de même le pipeline avec les instcrutions qui auront probablement lieu (ce qui complexifie encore l'unité de contrôle qu doiot faire des prévisions statistique). Si le saut n'a pas été prévu, il faut vider et recharger le pipeline => perte de tems et difficulté à prévoir durée 'exécution d'un programme

**diapo XY** Processeur superscalaire
Plusieurs unités fonctionnelles (ALU, FPU...) dans la même CPU pour exécuter plusieurs instrcutions à la fois. Avant, un calcul sur des entiers en même temps qu'un calcul sur des réels. Maintenant, il y a même plusieurs calculs sur des entiers ossibles en même temps.
voir le Zen 3

Exécution dans le désordre : on exécute une instruction dès que possible. (out of order execution). 
Le taux de prédiction est très bon et rares sont les insctructions exécutées et non utilisées.

**diapo XZ** Hyperthreading
Pour remplir les trous dus aux ruptures de séquences et aux attentes de données, on exécute 2 threads en même temps sur une même CPU.
Séquenceur plus compliqué car il ne faut pas mélanger les registres.
2 processeurs logiques pour un seul coeur physique. Le microprocesseur présente 2 CPU logiques au système d'exploitation.

**diapo YX** Multicoeur
Attention, ce n'est pas si simple, il faut paralléliser le programme, tenir compte des temps de transfert en passant par la cache de niveau 3.
Evidemment pour le multitâche, c'est top.
Les processeurs étant sur la même puce, on gagne en vitesse de transfert entre les CPU.

Conclure la partie par complexification de l'unité de contrôle, et 3 étages d'optimisation logicielles associées à ce matériel soft embarqué pour optimiser pipeline, compilateur et le développeur doit paaralléliser (openCL ?)

----------------------------------------------------------------

**Chipset**
Les contrôleurs de périphériques
En réalité, le processeur ne communique pas directement avec les périphériques :
ceux-ci sont reliés à des contrôleurs de périphériques et c’est avec eux que le processeur
dialogue. Un contrôleur peut gérer plusieurs périphériques et le processeur n’a pas
besoin de connaître précisément ces périphériques : s’il souhaite, par exemple, écrire
une donnée sur le disque dur, il le demande au contrôleur de disque dur et c’est ce
dernier qui se débrouille pour effectivement satisfaire la demande du processeur. Le
processeur transmet alors la donnée à écrire au contrôleur, le contrôleur la stocke dans
une mémoire tampon qu’il possède et la transmettra au disque dur

**Les bus de communication**
Le bus système, ou bus processeur, ou encore bus mémoire, souvent connu sous
le nom de FSB (Front Side Bus) permet de connecter le processeur à la mémoire
de type RAM ainsi qu’à d’autres composants (voir fig. 1.3). Les communications
sur ce bus système sont gérées par un composant particulier de la carte mère : le
NorthBridge. C’est ce composant qui conditionne souvent le chipset de la carte mère.
Les communications avec des périphériques plus lents tels que les disques durs par
exemple sont gérées par un autre composant : le SouthBridge.
Le bus de cache ou BSB (Back Side Bus) réalise la connexion entre le processeur
et la mémoire cache secondaire (cache L2 et cache L3) quand celle-ci n’est pas
directement intégrée au processeur.
Le bus PCIe (Peripheral Component Interconnect Express) permet la connexion de
périphériques variés comme une carte video,. . . Cette nouvelle technologie, compatible
avec l’ancien bus PCI, adopte un modèle de transmission par commutation de paquets
(très proche du modèle réseau TCP/IP) sur une ligne série à la différence de l’ancien
protocole parallèle dans lequel un composant monopolisait le bus pendant sa 


Intel Virtualization Technology (Intel VT-x) ?

QPI processeur - Chipset



**diapo** Alder Lake
10nm Enhanced Superfin CPU series
Hybrid Technology with small (high-efficiency) and big cores (high-performance) (8 Golden Cove big cores and up to 8 Gracemont small cores)
dual-channel DDR5-4800 memory
The CPU will have 16 PCI Gen5 capable lanes
125 W pour desktop

Donner la taille du bus adresse mémoire


BIOS ?
Le bios micro-programme de démarrage du PC, BIOS ou UEFI est stocké, au moins pour l'essentiel, sur une mémoire flash. Il peut être modifié, notamment pour les mises à jour.


**Conclusion**

système d'exploitation généraliste, multitâche pour les PC et smartphone et calcul pour les calculateurs.

**Bibliographie**
https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-004-computation-structures-spring-2017/
Chris Terman. 6.004 Computation Structures. Spring 2017. Massachusetts Institute of Technology: MIT OpenCourseWare, https://ocw.mit.edu. License: Creative Commons BY-NC-SA.

**exercices**
 Memoire donc tout binaire -> float ascii
adressage en octet

exo sequentiel / combin
exo taille des nombres

k Mo, Go...

Exercices	Un quizz sur volatile, non volatile et un ou 2 exos sur les associations de mémoire, les tailles, le nombre de bits d'adresses....


vmstat
dmidecode -t 17 | more
./mlc

reconstruction des données RAID5
