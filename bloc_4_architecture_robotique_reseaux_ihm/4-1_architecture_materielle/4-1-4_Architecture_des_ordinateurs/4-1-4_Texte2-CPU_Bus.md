# M4.1 Architecture

[//]: <> ()

## ch4 Architecture des ordinateurs

**diapo 4** Intro Objectif
Après avoir présenté le fonctionnement d'un système informatique minimaliste puis avoir détaillé certains aspects, notamment la pile et les interruptions à travers la présentation des systèmes embarqués, nous nous intéressons dans ce chapitre 4 aux ordinateurs.
Ces derniers sont des systèmes informatiques reprenant les éléments présentés dans les chapitres précédents.
Du PC portable du collégien aux puissants serveurs des data-center, les ordinateurs sont diffusés massivement et ont ainsi bénéficié de large financement pour leur développement. Ils ont ajouté à la base commune des systèmes informatiques des évolutions dans le but d'améliorer leurs performances.
Cette présentation se concentrera exclusivement sur cet objectif d'augmentation des performances, bien que d'autres problématiques d'encombrement ou de consommation électrique notamment conduisent à des progrès importants également en informatique. 
Ces 2 problématiques sont particulièrement fortes dans les ordinateurs portables et les smartphones, les récentes évolutions de ces derniers les rapprochant de plus en plus des premiers, comme l'a montré la présentation d'un SoC MediaTek dans le chapitre précédent.

Ce dernier chapitre sur l'architecture des PCs permettra à l'auditeur je l'espère de comprendre les spécifications techniques des machines pour faire des choix et les exploiter au mieux dans ses développements logiciels.

**diapos 5 et 6** Introduction
On reprend ici le système informatique minimal présenté dans les chapitres précédents, auquel nous avons ajouté quelques périphériques, en vert, pour se rapprocher de l'architecture des premiers ordinateurs. On distigue une mémoire centrale rapide mais volatile et une mémoire de stockage, plus lente mais non volatile (c'est-à-dire que les données y sont conservées lorsque l'alimentation est coupée). Dans le but d'améliorer les performances, ces PCs ont évolué, conduits essentiellement par les concepteurs de processeurs AMD, Intel et Nvidia et de mémoires Micron Technology et Samsung : 
(__CLIC__) Les CPUs des microprocesseurs se sont dotées d'instructions plus complexes, et de mécanismes comme les pipelines destinés à exécuter ces instructions plus rapidement.
(__CLIC__) Pour accélérer le fonctionnement des ordinateurs en parallélisant l'exécution des processus, Les concepteurs de processeurs ont inclus plusieurs CPU dans le même circuit intégré.
(__CLIC__) Sont aussi ajoutés sur certaines puces des coeurs spécialisés dans le rendu graphique, les GPU Graphic Processor Unit.
On voit ainsi qu'un circuit intégré (appelé aussi puce) Intel ou AMD contient bien plus qu'une simple CPU. Le terme SoC, System On Chip serait bien adapté. L'usage fait que l'on parle toujours de processeur pour désigner à la fois le circuit intégré mais aussi les coeurs qui sont à l'intérieur de celui-ci. Nous essaierons d'utiliser dans ce chapitre le terme micro-processeur pour le circuit intégré et le terme CPU pour les processeurs internes à celui-ci.
(__CLIC__) Sur les ordinateurs de bureau et les serveurs où place et consommation ne sont pas trop contraintes, il est possible d'ajouter une carte graphique avec un puissant GPU, sa mémoire et son refroidissement à l'extérieur du processeur, dialogant avec celui-ci par bus de données et bus d'adresse.
(__CLIC__) Ces bus ayant un débit limité au regard des vitesses des processeurs et les mémoires de grande capacité n'ayant pas gagné autant en rapidité que les processeurs, De la mémoire rapide est ajoutée sur la puce, comme sur les microcontrôleurs, au plus proche de la CPU. Cette mémoire, de petite taille, n'est qu'un tampon entre la CPU et la mémoire principale, elle est nommée mémoire cache.
(__CLIC__) Pour tirer partie au mieux des architectures Von Neuman et Harvard, et ainsi charger en même temps instructions et données, on ajoute 2 mémoires cache, une pour les données, une pour les instructions, pour chaque CPU. C'est l'architecture Harvard modifiée.
(__CLIC__) Enfin, les différents périphériques ayant des vitesses différentes, pour ne pas ralentir les plus rapides, un composant nommé Chipset est ajouté pour faire l'intermédiaire entre le processeur et ces périphériques, avec des bus adaptés pour chacun. Ce chipset intègre aussi les périphériques classiques : réseau et son notamment.

Ce chapitre détaillera ces différentes améliorations.


**diapo 7** Plan
Avant d'entrer dans les évolutions des processeurs et de présenter le chipset, les bus de communication et la carte mère,
(__CLIC__) Nous commencerons par détailler les différents types de mémoire d'un ordinateur.

-------------------------------------------------------------------------------------

**diapo 26 et 27** Intro au chapitre sur les améliorations de la CPU
Nous avons vu dans la partie précédente comment l'ordinateur, que ce soit un MAC ou un PC, exploitait les différentes technologies de mémoire SRAM, DRAM, flash et magnétiques pour la hiérarchie mémoire : registre - cache - mémoire centrale - mémoire de stockage, ceci afin d'optimiser les performances des processeurs.

(__CLIC__) Toujours dans le but d'améliorer les performances, la CPU des microprocesseurs est elle aussi l'objet de nombreuses innovations.
(__CLIC__) Nous verrons successivement comment les instructions complexes, les processeurs superscalaires, les pipelines, l'exécution non ordonnée et spéculative, le multi-threading et les processeurs multicoeurs ont continué à doper les performances des ordinateurs, malgré le mur de chaleur, en introduisant du parallélisme à tous les niveaux.

**diapo 28**
Le temps d'exécution d'un programme est égal au nombre d'instructions divisé par le nombre d'instructions par cycle horloge (IPC Instructions Per Cycle) multiplié par la fréquence d'horloge.

(__CLIC__) La vitesse d'horloge ne peut être raisonnablement augmentée au-delà de 5 GHz sans coûteux moyens de refroidissement. De plus, les temps de propagation à l'intérieur même du microprocesseur rendent délicat le fait de fonctionner à des fréquences plus élevées.
Pour fonctionner plus vite, on parallélise les exécutions à tous les niveaux.
(__CLIC__) On diminue le nombre d'instructions, ce qui se fait en créant des instructions complexes capables de traiter les nombres par blocs, c'est à dire en parallèle.
(__CLIC__) On peut aussi augmenter le nombre d'instructions par cycle horloge, ce qui est l'objectif annoncé de plusieurs technologies : pipeline et processeurs superscalaires joue sur l'exécution d'instruction en parallèle alors que les architectures multi-coeurs et multi-treading utilisent la mise en parallèle, réelle ou fictive de CPU.
Les processeurs modernes dépassent largement 1 instruction par coeur et par top horloge.

Il est toujours délicat de donner des chiffres, ces derniers dépendant de l'application pour laquelle ils ont été obtenus. Il faut les prendre comme un ordre de grandeur, avant de détailler les différentes innovations nommées ici.
(__CLIC__) Un Intel i9 de 12ème génération, conçu pour des PCs de bureau haut de gamme de 2021, avec 16 CPU, à 3.2 GHz approche les 100 000 MIPS pour de la compression ZIP, soit 31 instructions par cycle horloge pour le microprocesseur, et 1,9 instructions par cycle horloge par CPU. 
Pour le populaire test SGEMM de calcul de matrices avec des réels simple précision, on dépasse le Teraflops.
Notons que le processeur graphique (GPU) grand public haut de gamme Nvidia Geforce RTX3090 annonce 35 TFlops sur des calculs de réels simple précision.

**diapo 29** Instructions avancées 1
Nous avons vu dans le chapitre 2 qu'une machine de Turing universelle peut, à partir de quelques instructions, exécuter toute opération calculable, au prix d'un temps plus ou moins important. C'est ainsi qu'une division de deux réels par un processeur n'ayant que des blocs de calcul sur des entiers est possible mais très longue, cette division devant se traduire par plus de 1000 instructions sur des entiers.
(__CLIC__) Rapidement a donc été ajoutée à l'ALU une unité de calcul auxiliaire pour le calcul en virgule flottante (FPU Floating Point Unit).
Les calculs pour le rendu graphique utilisant massivement les matrices, sont apparus des unités de calcul sur des vecteurs, MMX sur les Pentium des années 2000 puis SSE (Streaming SIMD Extensions) et AVX (Advanced Vector Extensions), pour calculer sur des vecteurs également mais plus gros (__CLIC__).
Les instructions SSE, communes à Intel et AMD, permettent des calculs sur des nombres réels ou entiers 128 bits par 128 bits.
Les instructions AVX, Advanced Vector Extension utilisent des registres de 256 bits voir même de 512 bits pour les dernières versions. La multiplication de 16 nombres réels simple précision par 16 nombres réels est faite en un cycle instruction.

Les instructions avancées sont prises en compte par le compilateur. par exemple, si on fait une division de réels, le compilateur choisira d'utiliser l'instruction DIVISION du bloc FPU. Par contre, pour exploiter au mieux le parallélisme des instructions SIMD, la manière de programmer et d'organiser les données compte aussi. C'est pourquoi la mise en oeuvre des instructions SIMD est l'objet de la 3ème conférence, le calcul haute performance, à la suite de ce chapitre.

**diapo 29** Instructions avancées 2
Le schéma suivant présente l'intérêt des instructions SIMD qui permettent de paralléliser les calculs sur des réels. 
Une seule instruction SIMD sur des tableaux de 128 bits remplace 4 opérations scalaires sur des réels de 32 bits.
Des instructions SIMD toujours plus performantes continuent d'apparaître, gérant des tableaux 512 bits avec les dernières instructions AVX.

**diapo 30** Documentation AMD
L'introduction du manuel de programmation de l'architecture amd64, lisible pour un non spécialiste et très instructive, distingue 4 familles d'instructions : 
(__CLIC__) Les instructions d'usage général
(__CLIC__) Les instructions SIMD 
(__CLIC__) La compatibilité avec les instructions plus anciennes MMX, 3D Now! et x87.

Certaines de ces instructions complexes du jeu d'instruction amd64 sont parfois décomposées en plusieurs instructions simples.

C'est pourquoi on utilise parfois le terme **architecture apparente** pour celle qui est utilisée pour décrire le jeu d’instructions et d’architecture matérielle pour celle qui décrit effectivement le processeur.


**diapo 31** RISC / CISC

Les instructions avancées SIMD et la notion d'architecture apparente nous amènent à parler des catégories RISC et CISC, RISC signifiant Reduced Instruction Set Computer et CISC, Complexe Instruction Set Computer.
(__CLIC__) Un processeur RISC exécute toute instruction, relativement simple, en 1 seul cycle. Les instructions font la même taille, généralement 32 bits.
Le format des instructions est simplifié et le jeu d'instructions réduit.

(__CLIC__) L'architecture est plus simple, l'usage des pipelines que nous détaillerons ensuite, facilité mais le code comporte plus d'instructions.

Ces deux aspects limitent notamment les modes d'adressage et font qu'un chargement des registres est souvent nécessaire avant une opération qui utilisera ces derniers comme opérandes.
(__CLIC__) Les petits micro-contrôleurs de l'informatique embarquée avec leur jeu d'une cinquantaine d'instructions sont clairement de la famille RISC. ARM définit ses coeurs de processeurs de la famille A, utilisés notamment sur les portables APPLE, comme d'architecture RISC, avec notamment des instructions avancées SIMD travaillant sur 128 bits.

(__CLIC__) Sur un processeur CISC, certaines instructions sont complexes et plus longues que les autres, ce qui permet un code très court et très efficace, quite à ce que l'instruction nécessite plusieurs cycles instructions pour récupérer ses différents opérandes et stocker son résultat. 
(__CLIC__) Cette architecture était intéressante dans les années 80 car elle simplifiait le travail des compilateurs et limitait la taille du code. Par contre, les instructions CISC avec leur taille différente des instructions General Purpose, sont peu adaptées à l'usage des pipelines.
(__CLIC__) Les processeurs 486 notamment étaient d'architecture CISC. Les instructions complexes étaient décomposées par l'unité de contrôle en micro-instructions.

Depuis, les compilateurs ayant progressé et l'introduction des pipelines nécessitant des instructions de taille identique, les microprocesseurs CISC d'Intel et AMD ont laissé la place à des architectures plutôt de type RISC.
(__CLIC__) Cependant La rétro-compatibilité avec les anciennes architectures CISC et des instructions complexes font que certaines instructions dites "apparentes" sont décomposées en micro-instructions RISC par l'unité de contrôle de la CPU avant d'être exécutées.
C'est pourquoi Les documentations des nouveaux et complexes processeurs d'ordinateurs n'utilisent plus les termes CISC et RISC.

**diapo 33** Processeurs superscalaires.
Les instructions avancées montrent comment il est possible de paralléliser les données pour effectuer plusieurs calculs du même type en même temps.
A la vue de la CPU, on aussi imaginer optimiser son usage en envoyant simultanément des calculs dans l'unité de calcul de réel FPU et dans l'unité de calcul sur des entiers ALU.
(__CLIC__) **diapo 34**
Cela se fait tant et si bien que les concepteurs de processeurs ont profité de la meilleure finesse de gravure pour diminuer les empreintes des unités fonctionnelles FPU et ALU et ainsi ajouter des unités de calcul sur nombres réels et sur nombres entiers dans la CPU. 
(__CLIC__) On note la présence de blocs AGU spécialisés dans les additions d'entiers pour les calculs d'adresses mémoires, utiles notamment pour aller chercher rapidement les opérandes d'une instruction.
(__CLIC__) L'unité de contrôle, à qui l'exécution d'instructions complexes demandait la décomposition en microcode, doit de plus faire du dispatching pour envoyer les instructions sur les blocs appropriés.

L'utilisation de plusieurs unités de calculs d'une même CPU en parallèle caractérise un processeur supercalaire.
---------------------------------------------------------------------------------------
**diapo 35** Intro Pipelines
Nous avons vu dans la vidéo précédente que l'unité de contrôle a, avec les instructions avancées et les unités fonctionnelles multiples des fonctions supplémentaires de décodage et de répartition. Nous présentons ici une fonctionnalité avancée de l'unité de contrôle, le pipeline.

**diapo 36** Pipelines
La technologie des pipelines informatiques peut être illustrée par une chaîne de voitures extrêmement simplifiée. 
Plusieurs étapes de fabrication ont lieu en parallèle, sur des voitures différentes. Pendant que l'équipe de carrossiers installe la carrosserie de la voiture 1, des mécaniciens monte la transmission de la voiture 2, d'autres intègre le moteur dans la voiture 3 et l'équipe de chaudronniers soude le châssis de la voiture 1.
Dans une usine moderne, il faut 8h environ pour faire un modèle compact. En divisant ainsi la fabrication en tâches élémentaires, il sort de la chaine une voiture toutes les 10 mn.

**diapo 37** Pipelines 2
Sur un PC, l'exécution d'une instruction demande elle aussi plusieurs étapes, il faut d'abord aller chercher l'instruction en mémoire, Fetch en anglais, puis la décoder, charger les données (Load ou Fetch Data en anglais), exécuter l'instruction et ranger le résultat.
Si l'instruction et les données sont disponibles en mémoire cache ou dans les registres, chaque étape dure une période d'horloge CPU.
Certaines instructions, comme les instructions de branchement, n'ont pas de résultat à stocker et sont donc plus courtes à réaliser.

Les premiers processeurs et les petits microcontrôleurs ont ainsi besoin de plusieurs périodes d'horloge pour exécuter une instruction.

(__CLIC__) Comme pour les chaînes de montage automobile évoquées avant, l'interface avec la mémoire cache instruction étant disponible une fois l'instruction i chargée, on peut l'utiliser pour charger l'instruction i+1, dans un registre temporaire, à l'entrée du décodeur. 
(__CLIC__) De même, le décodeur d'instruction est disponible une fois l'instruction i décodée, pour décoder l'instruction i+1
(__CLIC__) Il en est de même pour le canal de communication avec la mémoire cache données
(__CLIC__) L'unité fonctionnelle
(__CLIC__) Le rangement des données utilisant aussi le canal de communication avec la mémoire cache, c'est un peu plus compliqué. Cependant, nous avons vu que ce canal a 4 à 8 voies de 64 bits.
(__CLIC__) On peu alors faire de même avec l'instruction 2 
(__CLIC__) Puis la 3
(__CLIC__) Et la 4.
(__CLIC__) Ainsi, en régime permanent, 5 étapes d'instructions réalisées en 5 périodes d'horloge ont lieu simultanément. On peut espérer une moyenne d'une instruction par période d'horloge.

**diapo 38** Pipelines 3 - dépendance de données
Les pipelines, très efficaces en cas de séquences d'instructions indépendantes, ne sont pas sans apporter de nouvelles difficultés.
La plus évidente est la dépendance de données. Si l'instruction i+1 utilise le résultat de l'instruction i, on ne peut remplir le pipeline naïvement. prenons l'exemple d'une addition stockant son résultat en R2 et d'une multiplication utilisant ce même R2. 
(__CLIC__) Avec notre pipeline à 5 étages, à t0 + 3.T_Clk, on chargerait R2 avant l'arrivée du résultat de l'addition. La multiplication se ferait en t0 + 4.T_Clk avec une donnée périmée.
Pour éviter cela, soit on introduit des instructions vides (NOP comme No Operation) entre l'addition et la multiplication, 
(__CLIC__) soit l'unité de contrôle ajoute des bulles dans le pipeline.
Chaque bulle dans le pipeline introduit inévitablement une perte de performance.
Pour limiter un peu le nombre de bulles, on peut passer directement le résultat de l'addition vers l'entrée du multiplieur, sans passer par R2, on appelle cela BYPASS, 

**diapo 39** Pipelines 3 - dépendance de contrôle
Un autre problème des pipelines est lié aux structures de contrôle de flux, caractérisée dans les langages haut niveau par les structures if, while, for.
Lors d'un saut, l'instruction suivante à exécuter n'est pas celle située à l'adresse suivante en mémoire.
Une première solution est, à l'arrivée dans le bloc FETCH d'une instruction de saut, de geler le pipeline en attendant le résultat de la condition du saut, c'est évidemment très pénalisant pour les performances.
Une autre solution est l'ajout d'une unité de prédiction dans l'unité de contrôle de la CPU.
Avant de remplir le pipeline, l'unité de contrôle de la CPU utilise l'unité de prédiction qui, pour les sauts non conditionnels, indique l'adresse de l'instruction suivante et qui, pour les sauts conditionnels, fait des prédictions avec l'historique du saut pour charger l'instruction suivante la plus probable. 
Une fois l'instruction de saut exécuté, on vérifie si la prédiction était bonne. Si elle l'était, on continue, sinon, on vide le pipeline et on charge les bonnes instructions. En moyenne, les taux de prédiction exacte sont supérieurs à 95%, d'où une conservation presque intacte de la performance des pipelines malgré les sauts.
L'unité de prédiction des processeurs modernes est un véritable processeur spécialisé au service de l'unité de contrôle.

(__CLIC__) En cas d'interruption, liée à un problème d'exécution (accès à une zone mémoire interdite ou division par zéro par exemple) ou à un événement extérieur à la CPU (fin d'une lecture de secteur sur le disque ou caractère tapé au clavier par exemple), il n'y a d'autre solution que de vider le pipeline.

**diapo 40** Processeurs superscalaires à exécution non ordonnée
Nous venons de voir comment sur un processeur ayant une unité fonctionnelle, le pipeline permettait d'augmenter considérablement l'efficacité en augmentant le nombre d'instructions exécutées par période d'horloge.
(__CLIC__) Ajoutons à cela la présence de plusieurs unités fonctionnelles.
(__CLIC__) On peut alors ajouter un pipeline par unité fonctionnelle, les instructions étant envoyées dans l'un ou l'autre pipeline en fonction de leur nature. Une fois les instructions exécutées dans des unités fonctionnelles parallèles, il est important de réordonner les résultats.
(__CLIC__) Considérons maintenant la prédiction.
(__CLIC__) La prédiction et la remise en ordre permettent d'envisager d'exécuter des séquences dans le désordre, pour mieux optimiser l'usage des différentes unités fonctionnelles.

(__CLIC__) L'exécution dans le désordre désigne le fait d'éviter les bulles dans le pipeline en choisissant d’exécuter des instructions indépendantes des premières à l’avance et de stocker le résultat dans un buffer, dans l’attente du moment pour le resortir.

(__CLIC__) L'exécution spéculative consiste à exécuter une séquence d’instructions sans avoir la certitude qu’on entrera dans cette séquence. Les résultats sont là aussi stockés dans un buffer en attente du moment où les instructions apparaîtraient.

**diapo 41** Processeurs superscalaires à exécution non ordonnée
On peut schématiser ces nouvelles fonctionnalités de l'unité de contrôle en enrichissant le schéma d'une CPU précédent. J'ai rajouté le contrôleur d'interruption APIC pour Advanced Programmable Interrupt Controller, gérant l'arrivée des interruptions, leur masquage, leur priorité et les adresses des fonctions d'interruptions à exécuter.
L'unité de contrôle a gagné de nouvelles fonctionnalités : En plus d'aller chercher les instructions et de les décoder, elle doit aussi décomposer en micro-code les instructions complexes et prédire les branches susceptibles d'être exécutées.
Sur les Intel Core 2021 ou les AMD Zen 3, la liaison avec le cache instruction permet de lire 32 octets simultanément.
L'unité de contrôle décode plusieurs instructions en parallèle (6 sur ces mêmes processeurs 2021).
A cette unité de contrôle appelée aussi Front-end - extrémité amont - de la CPU s'ajoute une file d'attente et un ordonnanceur chargé d'envoyer les instructions dans les différentes unités fonctionnelles. Les unités AGU de calcul d'adresses permettent d'aller cercher les données en mémoire cache données. Celle-ci utilise le cache TLB pour traduire les adresses virtuelles en adresses physiques.
(__CLIC__) Pour assurer une apparition des données dans l'ordre en mémoire cache données, l'unité d'ordonnancement utilise un buffer de remise en ordre et une mémoire de stockage "Store".

**diapo 42** Architecture Zen 3
Nous avons ainsi réuni l'ensemble des blocs d'une CPU moderne. AMD propose le diagramme bloc suivant de son coeur ZEN 3 utilisé sur les microprocesseurs Ryzen et Epyc.
(__CLIC__) On retrouve la mémoire cache instruction, avec 8 voies 64 bits d'échange avec l'unité de contrôle.
(__CLIC__) L'unité de contrôle - Front end, est chargée de récupérer les instructions dans la mémoire cache et de les décoder. Elle peut décoder jusqu'à 6 instructions par cycles.
(__CLIC__) L'unité de prédiction dispose d'une mémoire cache importante pour stocker les séquences d'instructions à exécuter.
(__CLIC__) Chez AMD, à la différence de Intel, il y a plusieurs blocs d'ordonnancement Scheduler, et un répartiteur dispatch qui envoie les instructions sur un bloc ou sur l'autre.
(__CLIC__) Des registres différents sont disponibles pour les unités fonctionnelles travaillant sur des entiers et celles travaillant sur des réels.
(__CLIC__) Les unités fonctionnelles travaillant sur des entiers comprennent entre autres 4 ALU pour les calculs divers et 3 unités de calcul d'adresses.
(__CLIC__) Une mémoire stocke les données pour les envoyer en ordre dans la mémoire cache
(__CLIC__) La mémoire cache donnée de niveau 1 dispose de 8 voies comme la mémoire cache instructions et la mémoire cache de niveau 2.
(__CLIC__) La traduction d'adresses virtuelles en adresses physiques est faite par un buffer TLB de 64 entrées pour les pages d'instructions et les pages de données au niveau 1 et jusqu'à 2000 pour le niveau 2.

**diapo 43** Architecture Coeur performance de Intel
Dans le diaporama du Architecture Day 2021 de Intel, on retrouve sur le coeur Performance des derniers processeurs les mêmes éléments, 
(__CLIC__) une mémoire cache instruction avec un buffer de traduction d'adresse,
(__CLIC__) une unité de lecture des instructions et décodage, capable de 6 décodages par cycle instruction.
(__CLIC__) Elle est complétée d'une unité de prédiction
(__CLIC__) Les instructions décodées sont alors placées dans la file d'attente où un seul ordonnanceur les récupère pour les affecter, pas forcément dans l'ordre, à des unités fonctionnelles.
(__CLIC__) Celles-ci compte notamment 5 unités arithmétiques et logiques scalaires et des instructions sur vecteurs et même sur matrices, les instructions AMX.
(__CLIC__) Des blocs spécifiques sont réservés pour le calcul des adresses et le chargement et stockage des données.
(__CLIC__) Enfin, on trouve une mémoire de stockage intermédiaire
(__CLIC__) Avant le dépôt des résultats ordonnés dans la mémoire cache données.


-------------------------------------------------------------------------------
**diapo 44** Intro Multithread multi
Le traitement en paralléle des données grâce aux instructions SIMD et l'exécution en parallèle des instructions sur les processeurs superscalaires à exécution non ordonnée ont permis une augmentation importante des performances des CPU. Nous allons voir dans cette vidéo la dernière amélioration CPU de ce chapitre avec la mise en parallèle des processus sur des coeurs virtuels ou réels, puis nous dirons un mot du refroidissement.

**diapo 45** Multi-threading
Dans le module suivant sur les systèmes d'exploitation, nous reviendrons en détail sur les processus et les processus légers, thread, en anglais. En quelques mots et de manière très simplifiée, sur un ordinateur, plusieurs processus sont exécutés quasiment simultanément. Le système d'exploitation attribue une CPU à un processus pendant un quanta de temps (10 ms typiquement) puis à un autre, etc.
A chaque changement de thread sur une CPU, le contexte de celle-ci, c'est-à-dire l'ensemble des registres, doit être sauvegardé dans la zone de mémoire du processus pour être rechargé lorsqu'il aura de nouveau accès à la CPU. Ce changement de contexte est coûteux en temps processeur et doit être minimisé pour de bonnes performances.

Nous avons vu dans les 2 vidéos précédentes comment une CPU utilisait au mieux ses ressources via les pipelines et les unités fonctionnelles exécutant des instructions en parallèle. Cependant, lors d'un défaut de cache, l'attente pour obtenir la donnée ou les instructions suivantes peut aller jusqu'à 150 cycles instructions. Que faire pendant ce temps ?

Pour remplir ces temps d'inoccupation dus aux ruptures de séquences et aux attentes de données, les concepteurs de CPU ont ajouté quelques registres pour pouvoir exécuter 2 threads en même temps sur une même CPU, ce que l'on nomme Simultaneaous Multi-threading SMT. 
(__CLIC__) Ainsi, en cas de défaut de cache d'un des threads, l'autre continuera d'exploiter les ressources de la CPU.
(__CLIC__) Sont ainsi ajoutés des registres contrôle : Stack Pointer, Program Counter et Status notamment,
(__CLIC__) une file d'attente des instructions à exécuter et un buffer de remise en ordre.
(__CLIC__) Par ailleurs, our faciliter l'utilisation des différentes unités fonctionnelles lors des exécutions non ordonnées, l'ordonnanceur renomme les registres à utiliser et stocke la correspondance dans une table de renommage.
(__CLIC__) Ainsi, pour utiliser un 2nd thread sur la même CPU, il suffit d'ajouter une table de renommage propre au 2nd thread, l'ordonnanceur veillant à utiliser au mieux les registres, sans interférence entre les 2 threads.

En moyenne, sur les coeurs AMD et Intel offrant tous 2 cette technologie de multi-threading à processeurs logiques pour un seul coeur physique, le microprocesseur présente 2 CPU logiques au système d'exploitation et gagne +5 à +30% en performance suivant les applications.

**diapo 46** Multi-threading 2
Chez Intel la technologie SMT est nommée Hyper-Threading, avec comme chez AMD 2 threads par CPU.
(__CLIC__) La commande lscpu sur mon PC à microprocesseur i7 pour portable affiche 8 processeurs pour seulement 4 CPU.
(__CLIC__) Sur une machine plus puissante à processeur Xeon, lscpu affiche 24 cpu pour seulement 12 coeurs physiques.
Le gain du multi-threading étant loin de celui du doublement des CPU, on peut mettre en question la légitimité de l'affichage du doublement du nombre de processeurs.
Les performants System On Chip de smartphone et les microprocesseurs Apple basés sur des coeurs ARM sont tous Single-Thread et affichent des performances comparables à leurs équivalents Intel ou AMD.

On voit ainsi arriver les limites de l'optimisation des CPUs avec la parallélisation des données et des instructions, jusqu'à exécuter 2 tâches simultanéments sur le même coeur. La fréquence ne pouvant augmenter avec un refroidissement raisonnable, 
(__CLIC__) la solution pour augmenter les performances a été de grouper plusieurs CPU ou coeurs sur la même puce. Ce sont les microprocesseurs multi-coeurs.

**diapo 47** Multicoeur
A la différence des architectures à plusieurs processeurs existant avant les multicoeurs, les différentes CPU partagent une mémoire cache de niveau 3 commune à la latence beaucoup plus faible que la mémoire centrale.
Cette mémoire cache de niveau 3 devient alors multi-accès. Elle est accessible par les différentes CPU et par le contrôleur de mémoire.
La gestion des accès multiples à la mémoire cache et des incohérences liées aux modifications dans les différentes cache de niveau 2 fait l'objet d'une gestion complexe de la mémoire cache.

**diapo 48** Comme exemple de processeur multicoeur, présentons l'architecture Alder Lake, issue du diaporama de l'Architecture Day 2021 de Intel.

En utilisant des coeurs ARM, venu du monde l'embarqué, les concepteurs de SoC pour smartphone ont fait très sobre et pourtant très performant. C'est pourquoi auourd'hui Apple et Qualcomm arrivent sur le marché des ordinateurs portables avec cette expérience smartphone.

En réponse à cette recherche de performance associée à une consommation moindre, Intel propose une architecture hybride pour ses nouveaux microprocesseurs, nommés System On Chip dans le document. 
Effectivement, ce qui reste appelé microprocesseur sur le site d'Intel comprend des CPU orientées performance, mais aussi des CPU orientées rendement à la consommation moindre et un GPU, de taille variable, 2 fois plus gros pour les versions destinées aux ordinateurs portables, sans carte graphique, que pour les versions destinées aux ordinateurs de bureau, qui pourraient en ajouter une si besoin.
La mémoire cache partagée entre les CPU annonce 1000 Goctet/s de débit.

Le système d'exploitation, si il prend en compte cette architecture, peut alors orienter les tâches urgentes et gourmandes en calcul vers les processeurs "performance" et les tâches usuelles vers les processeurs "rendement". On voit ici le lien très fort entre le système d'exploitation et l'architecture du microprocesseur.

**diapo 49 à 51** GPU
Nous n'avons traité jusqu'à présent que des CPU généralistes.
Certaines CPU sont spécialisées dans le calcul vectoriel, avec de nombreux blocs de calculs parallèles. C'est le cas des GPU Graphic Processor Unit, conçus à l'origine pour les calculs de rendus d'affichage 3D, mais utilisés aussi aujourd'hui pour le calcul haute performance. On prend ici l'exemple du GPU très haut de gamme Nvidia A100.
Le coeur de base du GPU contient 16 unités calculs sur entiers, 16 sur réels simple précision 32 bits et 16 sur réels double précision 64 bits, plus un coeur Tensor opérant sur des matrices 4x4 de réels 16 bits.

(__CLIC__) Le Streaming Multiprocessor contient 4 coeurs précédemment décrits, ainsi que de la mémoire cache de 2nd niveau, commune aux quatre coeurs.

(__CLIC__) Le circuit intégré complet comprend 108 de ces streaming processors, ce qui lui autorise des performances remarquables d'une centaine de Tflops, à comparer avec les 1 TFlops du pourtant très performant intel core i9 12ème génération.
En contrepartie, il consomme 400 W en fonctionnement à haut régime et ne peut exécuter d'autres opérations que les additions et multiplications pour lesquelles il est optimisé.

La présence de plusieurs CPU et du multithreading est très utile pour un usage classique de l'ordinateur où le système d'exploitation doit gérer plusieurs processus indépendants demandant des ressources CPU.
Par contre, pour des calculs performants, l'utilisation de multiples CPU, qui plus est celles d'un GPU, demande de paralléliser le calcul.
Pour cela, le développeur aidé de son compilateur, voir d'un langage spécial comme OpenCL, doit prendre en compte l'architecture dans son code.
La description détaillée des GPU et une introduction à leur programmation sera l'objet de la 2nde conférence, consacrée aux GPU, qui suit ce chapitre.

**diapo 52** Refroidissement
Si les microprocesseurs de PC portables ont des consommations raisonnables, les GPU comme les microprocesseurs de station de travail consomment beaucoup et demandent à être refroidis. Le GPU GA100 de Nvidia consomme jusqu'à 400 W et un processeur d'usage généram intel i9-12900 K consomme jusqu'à 240 W en mode Turbo, on approche les 100 W par centimètre carré. Rappelons qu'une plaque électrique du commerce fournit 7W par cm².
Pour un refroidissement performant, des caloducs utilisent le changement de phase d'un fluide (de l'eau le plus souvent) pour évacuer le maximum de chaleur vers les ailettes mécaniques ventilées.


-----------------------------------------------------------------------------------
**diapo 53** Intro bus de communication
Lors du chapitre sur les mémoires, nous avons vu que pour éviter d'être ralentie par des périphériques plus lent, la mémoire centrale communiquait via des canaux spécifiques, parfois même en liaison point à point avec le microprocesseur, dans le cas de 2 canaux et 2 barettes mémoire.
Les périphériques restants, interne ou externe, disque dur, SSD, carte graphique, clavier, souris, réseaux, ont aussi des besoins et des capacités très différents. C'est pourquoi ils utilisent des canaux de communication différents.
(__CLIC__) La présentation des communications du microprocesseur avec les éléments de son environnement est l'objet de la dernière partie de ce chapitre.

**diapo 54**
Commençons par préciser les termes et concepts propres à la communication avec ces périphériques.

(__CLIC__) Un bus est un canal de communication permettant de relier plus de 2 nœuds. Le protocole de communication doit gérer la prise de parole sur le bus. Le bus s’adapte au plus lent des nœuds. On trouve souvent 3 bus pour une communication : adresses, données et contrôle (les signaux Chip Select, Read, Write...)

(__CLIC__) Une laision point à point est un canal de communication entre 2 nœuds. La gestion de la prise de parole est simplifiée : soit via 2 canaux (1→2 et 2→1), soit en ayant un maître et un esclave.
(__CLIC__) Sur un Bus ou une liaison parallèle, plusieurs bits circulent simultanément sur le canal à plusieurs conducteurs. C'est le cas de l'ancien bus IDE de connexion des disques durs, dont une nappe est représentée ici. 
(__CLIC__) Sur un Bus ou une liaison série, Les bits sont envoyés les uns après les autres, comme sur la liaison SATA de connexion des disques qui a remplacé le bus IDE.

Sur les bus parallèles, les problèmes de différence de marche, de bande passante et de diaphonie (interférences d'une voie sur une autre) réservent cette topologie aux bus système entre le CPU et la mémoire.
Les autres bus sont tous passés au format série. PCI est devenu PCIexpress, le port parallèle a été remplacé par l'USB et, comme nous l'avons dit le bus IDE a été remplacé par le SATA.
Cependant, les bus à proprement parler, avec plusieurs noeuds sur les mêmes conducteurs série, cela ne fonctionne pas bien en très haute vitesse. C'est pourquoi ils sont remplacés par des liaisons point à point, comme le SATA ou le PCIexpress. L'USB est un peu différent puisqu'il autorise aussi le chaînage, utilisé pour les hub USB, même si il est essentiellement utilisé lui aussi en point à point.

(__CLIC__) Le DMA, Direct Memory Access, est le Transfert direct, initié par le processeur, de données d’une zone mémoire à une autre. Les sources et destination peuvent être des mémoires de stockage, des mémoires centrales, des périphériques.

(__CLIC__) Enfin, le Chipset ou Platform Controller Hub (PCH) est le Composant chargé de coordonner les communications plus lentes du processeur.

**diapo 53**
L'architecture des ordinateurs est dictée par le fabricant du microprocesseur. Les architectures AMD et Intel, pour MAC comme pour PC, sont très proches. Gageons que les nouvelles architectures des ordinateurs Apple, construites autour du nouveau microprocesseur M1 resteront assez proches de celles des PCs, pour pouvoir s'appuyer sur les mêmes composants mémoires et cartes graphiques notamment.
C'est pourquoi notre description s'appuiera sur l'architecture proposée par Intel autour de ses microprocesseurs grand public Intel Core de 12ème génération.
Le microprocesseur est vendu dans un boitier métallique à 1700 contacts dessous.

(__CLIC__) 300 de ces broches environ sont consacrées au bus système pour la mémoire, contrôlé par 2 IMC (Internal Memory Controller) sur le microprocesseur). Il y a 2 voies de mémoire double canal. Les canaux entrelacés, comme le RAID0 permettent d'améliorer le temps d'accès à la mémoire.
(__CLIC__) Le microprocesseur possède également un contrôleur PCIexpress, ce qui lui permet de proposer également une communication PCIexpress 5.0 avec 16 voies séries. 16 voies série, ce n'est plus vraiment de la liaison série ! Ce port est prévu pour limiter la latence et augmenter le débit des échanges avec la carte graphique, son GPU et sa mémoire embarquée.
(__CLIC__) Toujours en PCIexpress, mais 4.0, il est possible d'utiliser 4 voies pour connecter un SSD NVMe.
(__CLIC__) Enfin le microprocesseur, pour ses versions équipées d'un GPU interne, propose également la connectivité pour 4 écrans via DisplayPort ou HDMI.

(__CLIC__) Pour les périphériques plus lents, le microprocesseur, ou plutôt le SoC au vu du nombre de contrôleurs de périphériques qu'il propose, utilise un intermédiaire connecté par une liaison rapide DMI 4.0 à 8 voies : c'est le chipset ou PCH : Platform Controller Hub.

Ce composant gravé avec les meilleurs technologies est l'indispensable complément au microprocesseur pour un ordinateur. En plus de la gestion de l'horloge temps réel et des convertisseurs d'entrées/sorties Audio, il possède de nombreux contrôleurs de communication pour discuter avec tous les périphériques :

(__CLIC__) Le PCIexpress permet de connecter des périphériques rapides comme un second SSD.
(__CLIC__) Les 4 ports SATA autorisent la connexion de 4 disques SSD ou magnétiques, avec la possibilité de les associer en RAID 0,1 ou 5.
(__CLIC__) l'USB 3.2 atteint aujourd'hui 20 Gb/s, ce qui permet de remplacer nombre de cartes métiers d'acquisition internes par des convertisseurs USB externes.
(__CLIC__) Les ports USB 2 moins performants sont suffisants pour les périphériques classiques : souris, clavier, imprimante, scanner.
(__CLIC__) Le Chipset possède également les contrôleurs pour les bus électroniques permettant de communiquer avec d'autres coomposants sur la carte comme les capteurs de températures par exemple. Le port série UART sort encore, sur une prise DB9, sur certains PC.
(__CLIC__) Le contrôleur WIFI du Chipset ne nécessite en plus que le composant gérant la couche physique et une antenne.
(__CLIC__) De même le contrôleur Gigabit Ethernet ne nécessite qu'un composant de gestion de la couche physique et une prise.
(__CLIC__) Enfin, Thunderbolt est très utilisé par les stations d'accueil des ordinateurs portables.

**diapo 54** Protocoles de communication 1
DMI, Direct Media Interface est le protocole de dialogue MicroProcesseur - Chipset propriétaire Intel, assez proche de PCIexpress, son débit de 8 Gb/s par voie permet ici d'atteindre, avec 8 voies, 64 Gbit/s.

PCIe ou PCIexpress, Peripheral Component Interconnect Express, n'est pas un bus comme PCI mais une liaison série point à point, avec 1 à 16 voies composées d’une paire de fils torsadée. Avec 16 voies, le débit du PCIe 5.0 peut atteindre 512 Gbit/s, d'où la nécessité d'intégrer le contrôleur PCIe 5.0 dans le micro-processeur.

DisplayPort et HDMI sont deux interfaces de communication entre PC et écrans. DisplayPort propose 80 Gbit/s, un connecteur avec un biseau et surtout, ce qui fait son point fort, est aussi transporté par l’USB C.

Le HDMI, High-Definition Multimedia Interface, utilise un connecteur avec 2 biseaux et a un débit allant jusqu'à 48 Gbit/s.

**diapo 54** Protocoles de communication 2
M.2 est un Connecteur comportant les bus SATA, USB 3.0 et PCIe 4.0, présent sur les cartes mères pour les SSD.

NVMe, Non-Volatile Memory express, est la spécification d’interface des disques SSD via PCIexpress 4.0. Son débit théorique maximal est de 64 Gbit/s. Les disques NVMe utilisent un connecteur M.2.

SATA, Serial Advanced Technology Attachment, est une liaison série point à point utilisée pour les disques durs magnétiques essentiellement, à 6 Gbit/s. En effet, son débit est insuffisant pour exploiter au mieux les SSD modernes.

Le bien connu USB est un bus série permettant de connecter les périphériques « à chaud ». Les périphériques sont de plus automatiquement identifiés, ce qui simplifie grandement son usage par les particuliers. Le débit atteint 20 Gbit/s pour l’USB 3.2 gen 2x2. L’USB4 annonce 40 Gbit/s. Les connecteurs sont nombreux : USB A sur les PCs et USB C sur les PC et smartphone aujourd'hui.

Thuderbolt 4 est une interface comprenant 4 voies PCIe 3.0, 8 voies DispalPort et une alimentation bi-directionnelle de 100W, le tout sur un connecteur USB C. Il est très utilisé pour les stations d'accueil de PC portables.


**diapo XX** PC DELL
Regardons maintenant l'implémentation physique de cette architecture. Sur le serveur DELL, on peut voir (__CLIC__)  l'alimentation 1000 W.
Le circuit imprimé dans le fond est la carte mère, élément central d'un ordinateur.
(__CLIC__) Sur cette configuration puissante, elle accueille deux microprocesseurs et le Chipset.
Le chipset possède son dissipateur passif.
(__CLIC__) Les processeurs ont, eux, un dissipateur important avec caloduc et ailettes de refroidissement.
(__CLIC__) La configuration de serveur retenue par DELL pour cette illustration comprend de nombreuses barettes SDRAM de mémoire centrale.
(__CLIC__) Pour accélérer les calculs, les microprocesseurs sont assistés par 3 cartes graphiques. Avec 3 cartes graphiques, Les GPU sont utilisés pour du calcul, ce qui montre l'usage non exclusivement graphique des GPU.
(__CLIC__) La connectique en face arrière, complétée par celle de la face avant, tire partie de la diversité des protocoles gérés par le chipset.

Intéressons-nous de plus près à la carte mère.

**diapo XY** Carte mère ASUS
Le concepteur de la carte mère n'a que peu de liberté, l'essentiel étant guidé par l'architecture dictée par les spécifications de l'ensemble {Microprocesseur, Chipset}. Il doit soigner les délicates connexions très rapides des bus mémoires et des PCIexpress. Il doit aussi travailler à la qualité de l'alimentation du microprocesseur et au bon refroidissement de l'ensemble.
Observons la carte mère proposée par le spécialiste ASUS pour l'ensemble Intel Core 12ème génération et Chipset Z690.
On peut remarquer tout d'abord que la carte porte le nom du Chipset, (__CLIC__) soudé sur le circuit imprimé et caché sous un dissipateur thermique passif métallique. Le processeur peut lui être choisi dans la famille 12ème génération, tous sur Socket LGA1700. (__CLIC__). L'espace autour du socket et les fixations servent à installer le puissant système de refroidissement, capable d'évacuer les 125W nominaux du microprocesseur.

(__CLIC__) Les différents connecteurs d'alimentation permettent d'accueillir la puissance électrique, en 12, 5 et 3,3V issue de l'alimentation standard du boîtier. Celle-ci servira à alimenter le processeur, le Chipset et les mémoires mais aussi les périphériques USB ou Thunderbolt. Les cartes graphiques très gourmandes ont souvent un connecteur d'alimentation spécifique.
(__CLIC__) Sur la carte, des régulateurs vont adapter cette alimentation au microprocesseur. Les spécifications Intel indique qu'il doit être alimenté en 1,72V et peut demander en pic jusqu'à 280 A !
(__CLIC__) Au plus près du microprocesseur, on retrouve les 4 emplacements pour les barettes DDR5 de la mémoire centrale. Un zoom sur la carte permet de voir le soin apporté à ce bus extrêmement rapide.
(__CLIC__) Egalement connecté directement au microprocesseur, le slot M.2 pour la mémoire de stockage SSD NVMe propose même un dissipateur thermique passif.
(__CLIC__) Dernier connecteur d'extension relié au microprocesseur, le slot PCIexpress principal est celui destiné à accueillir une éventuelle carte graphique. Les premières cartes exploitant la version 5.0 de PCI express sont annoncées.
(__CLIC__) En passant par le Chipset Z690, le microprocesseur a accès à deux autres slots M.2 pour SSD,
(__CLIC__) Il a accès également à 4 connecteurs PCIexpress dont certains PCIexpress 4.0 12 voies.
(__CLIC__) S'y ajoutent 4 connecteurs SATA 6 Gb/s pour des disques supplémentaires, SSD ou magnétiques.
(__CLIC__) La large connectivité permise par le Z690 est déportée à l'arrière du boîtier, cachée ici par le dissipateur thermique des régulateurs. On y retrouve USB type C, USB 3.2 sur connecteur USB A, la connectique audio, vidéo (via les sorties vidéo du microprocesseur) et Ethernet.
(__CLIC__) Enfin, en bas de la carte, on distingue 2 petits circuits de mémoire flash, utilisés pour stocker le BIOS UEFI.
La connectique Tunderbolt est possible via une carte PCIexpress optionnelle.

**diapo XZ** Carte mère ASUS de PC portable
On retrouve sur cette carte mère de PC portable Asus Expertbook B7 une architecture similaire exploitant les processeurs i7 de 11ème génération. Contrairement à la carte mère précédente, le système de refroidissement est sur la photo.
(__CLIC__) On y voit bien les caloduc évacuant la chaleur du microprocesseur vers les ailettes ventilées. 
(__CLIC__) Le microprocesseur version mobile, moins gourmand, est fourni avec le chipset sur un circuit imprimé spécifique nommé die. Ce circuit est caché ici par son système de refroidissement.
(__CLIC__) On retrouve les mémoires, en version SODIMM, plus compacte, toujours au plus près du microprocesseur.
(__CLIC__) Le SSD NVME, très compact, est évidemment apprécié sur les ordinateurs portables.
(__CLIC__) On trouve enfin la connectique, USB 3 et thunderbolt 4 notamment, ce dernier étant utilisé pour connecter les portables aux stations d'accueil. D'autres connecteurs USB et d'affichage sont présents de l'autre côté de la carte.

**Conclusion**
Nous venons de voir les nombreuses interfaces, toujours plus rapides, de l'ordinateur.
Avant, nous avons vu différentes innovations ayant permis d'atteindre les très hautes performances actuelles des processeurs : 
L'utilisation de mémoire cache accélère les échanges d’instructions et de données avec le processeur
La Parallélisation du traitement des données (via les instructions SIMD) et la parallélisation de l’exécution des instructions (processeurs superscalaires, pipeline et dérivés), ainsi que le multithreading permettent d'optimiser l'usage de la CPU
Enfin, l'usage de processeurs multi-coeurs a permis de continuer à faire croître les performances.
Pour exploiter au mieux ces ressources, le lien est très étroit entre cette architecture matérielle et le Système d’exploitation, thème du module suivant.
Avant cela 3 conférences permettront d'une part de découvrir une architecture différente qui prend de l'ampleur dans le monde de l'informatique, les FPGA. Ensuite, seront présentés les GPU et leur programmation puis les instructions SIMD et leur mise en oeuvre.

**Bibliographie**
https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-004-computation-structures-spring-2017/
Chris Terman. 6.004 Computation Structures. Spring 2017. Massachusetts Institute of Technology: MIT OpenCourseWare, https://ocw.mit.edu. License: Creative Commons BY-NC-SA.
Intel_64_and_IA32_Architectures_Software_Developer_s_Manual.pdf
https://download.intel.com/newsroom/2021/client-computing/intel-architecture-day-2021-presentation.pdf
Architecture et Technologie des ordinateurs, Paolo Zanella, Yves Ligier et Emmanuel Lazard, Dunod, 2017
Wikipédia, la version anglaise est très complète sur le sujet, avec des illustrations aux licences creative commons.
https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-004-computation-structures-spring-2017/c14/c14s1/
https://www.intel.com/pressroom/kits/quickreffam.htm
Section Architectures matérielles des Techniques de l’Ingénieur

La méthode scientifique : Y a-t-il une vie après la loi de Moore ? 
https://www.franceculture.fr/emissions/la-methode-scientifique/la-methode-scientifique-du-mercredi-23-janvier-2019

https://www.epi.asso.fr/revue/articles/a2109b.htm

Ordinateur quantique : https://www.universite-paris-saclay.fr/recherche/thematiques-et-structures/axes-et-grands-projets/quantum-centre-en-sciences-et-technologies-quantiques/lordinateur-quantique



Jacques Lonchamp, Introduction aux systèmes informatiques

**exercices**
 Memoire donc tout binaire -> float ascii
adressage en octet

exo sequentiel / combin
exo taille des nombres

k Mo, Go...

Exercices	Un quizz sur volatile, non volatile et un ou 2 exos sur les associations de mémoire, les tailles, le nombre de bits d'adresses....


vmstat
dmidecode -t 17 | more
./mlc

reconstruction des données RAID5

Exercice	des questions liées à des recherches dans le jeu d'instruction AMD64
lscpu sous linux pour afficher la mémoire cache du processeur
Sous windows : gestionnaire des tâches > performance ou 
		wmic cpu get L2CacheSize, L3CacheSize
