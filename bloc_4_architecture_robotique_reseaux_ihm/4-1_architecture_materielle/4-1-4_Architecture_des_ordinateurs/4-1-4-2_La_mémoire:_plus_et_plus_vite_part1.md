## Architecture des ordinateurs: La mémoire : plus et plus vite

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. Introduction
2. **La mémoire : plus et plus vite** (4 videos)
- **La hierarchie des mémoires, Les technologies SRAM et DRAM, les registres**
- **La mémoire centrale**
- La mémoire cache & la mémoire virtuelle (2 videos)
- Les mémoires de stockage (1 vidéo)
3. La CPU : calculer plus vite (2 vidéos)
4. Le chipset et les bus : communiquer (1 vidéos)

[![Vidéo 2 part1 B4-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S4-V2p1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S4-V2p1.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 2 part2 B4-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S4-V2p2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S4-V2p2.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
