## Architecture des ordinateurs: La mémoire : plus et plus vite

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. Introduction
2. **La mémoire : plus et plus vite** (4 videos)
- La hierarchie des mémoires, Les technologies SRAM et DRAM, les registres
- La mémoire centrale
- **La mémoire cache & la mémoire virtuelle** (2 videos)
- **Les mémoires de stockage** (1 vidéo)
3. La CPU : calculer plus vite (2 vidéos)
4. Le chipset et les bus : communiquer (1 vidéos)

[![Vidéo 3 B4-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S4-V3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S4-V3.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 4 B4-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S4-V4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S4-V4.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
