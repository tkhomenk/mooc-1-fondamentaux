## Introduction à l'architecture des systèmes informatique : De la machine de Turing au Pentium, un demi-siècle d'évolution sans limite

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. **Introduction**
2. **De la machine de Turing au Pentium, un demi-siècle d'évolution sans limite**
3. Croître malgré l'échauffement, les évolutions récentes de l'informatique (2 vidéos)

[![Vidéo 1 B4-M1-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S1-V1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S1-V1.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
