## Architecture des systèmes embarqués: Architecture d’un microcontrôleur, d’un SoC

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. Introduction. Applications et problématiques des systèmes embarqués
2. **Architecture d’un microcontrôleur, d’un SoC (2 vidéos)**
3. Les appels de fonctions et la pile
4. Les interruptions

[![Vidéo 2 part1 B4-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S3-V2p1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S3-V2p1.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 2 part2 B4-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S3-V2p2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S3-V2p2.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
