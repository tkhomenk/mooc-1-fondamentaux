## Architecture minimale d'un système informatique: Introduction. Du transistor au processeur

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. **Introduction. Du transistor au processeur** (3 videos)
2. Architectures Von Neumann / Harvard
3. Le langage de la CPU, l’assembleur
4. Du langage haut niveau à l’assembleur

[![Vidéo 1 B4-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S2-V1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S2-V1.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 2 B4-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S2-V2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S2-V2.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
