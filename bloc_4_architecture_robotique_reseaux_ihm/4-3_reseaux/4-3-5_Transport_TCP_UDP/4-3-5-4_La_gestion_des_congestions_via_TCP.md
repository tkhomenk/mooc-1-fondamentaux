# Transport TCP/UDP 4/4: La gestion des congestions via TCP

1. Présentation de la couche transport
2. Le protocole UDP
3. Le protocole TCP
4. **La gestion des congestions via TCP**

[![Vidéo 4 B4-M3-S5 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S5_video4_Congestion.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S5_video4_Congestion.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
