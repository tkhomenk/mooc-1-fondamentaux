# Routage 1/5: Le routage statique

1. **Le routage statique**
2. Le protocole NAT
3. Le routage dynamique - Le protocole RIP
4. Le routage dynamique - Le protocole OSPF
5. Le routage dynamique - Le protocole BGP

- Dans cette vidéo, **Anne Josiane Kouam** aborde les notions de **routeur, de table de routage et d'algorithmes de routage statique**

[![Vidéo 1 B4-M3-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S4_Video1_Routage_statique.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S4_Video1_Routage_statique.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
