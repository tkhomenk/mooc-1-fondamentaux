# Routage 2/5: Le protocole NAT

1. Le routage statique
2. **Le protocole NAT**
3. Le routage dynamique - Le protocole RIP
4. Le routage dynamique - Le protocole OSPF
5. Le routage dynamique - Le protocole BGP

- Dans cette vidéo, **Anne Josiane Kouam** nous parle du protocole particulier qu'est le **protocole NAT**.

[![Vidéo 2 B4-M3-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S4_Video2_ProtocoleNAT.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S4_Video2_ProtocoleNAT.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
